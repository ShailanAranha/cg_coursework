#pragma once
#include "CatmullRom.h"
#include "ControlPoint.h"

class ControlPointManager
{
public:

	/// <summary>
	/// Default constructor
	/// </summary>
	ControlPointManager();

	/// <summary>
	/// Destructor
	/// </summary>
	~ControlPointManager();

	/// <summary>
	/// Initlaize with data
	/// </summary>
	/// <param name="a_pCoinMesh"></param>
	/// <param name="a_CollisionDetectionManagerRef"></param>
	void Initialize(COpenAssetImportMesh* a_pCoinMesh, CollisionDetectionManager* a_CollisionDetectionManagerRef);

	/// <summary>
	/// CCatmullRom instance to create main spline
	/// </summary>
	CCatmullRom* m_CCatmullRomRef;

	/// <summary>
	/// CCatmullRom instance to create offset points
	/// </summary>
	CCatmullRom* m_CCatmullRomWithOffsetRef;

	/// <summary>
	/// List of main control points
	/// </summary>
	vector<ControlPoint> m_controlPoints;

	/// <summary>
	/// List of offset control points
	/// </summary>
	vector<glm::vec3> m_controlPointsWithOffset;

	/// <summary>
	/// Offset value for offset spline
	/// </summary>
	glm::vec3 m_offsetValue = glm::vec3(0, 2.5f, 0);


	/// <summary>
	/// Control point vertices
	/// </summary>
	glm::vec3 m_vec3Point1 = glm::vec3(150, 0, 80);
	glm::vec3 m_vec3Point2 = glm::vec3(200, -0.15, 80);
	glm::vec3 m_vec3Point3 = glm::vec3(250, 0, 80);
	glm::vec3 m_vec3Point4 = glm::vec3(300, 0, 0);
	glm::vec3 m_vec3Point5 = glm::vec3(250, 0, 0);

	glm::vec3 m_vec3Point6 = glm::vec3(150, 0, 0);
	glm::vec3 m_vec3Point7 = glm::vec3(50, 0, 0);
	glm::vec3 m_vec3Point8 = glm::vec3(30, 35, 0);
	glm::vec3 m_vec3Point9 = glm::vec3(0, 50, 0);
	glm::vec3 m_vec3Point10 = glm::vec3(-10, 50, 0);
	glm::vec3 m_vec3Point11 = glm::vec3(-30, 30, 0);
	glm::vec3 m_vec3Point12 = glm::vec3(-50, 35, 0);
	glm::vec3 m_vec3Point13 = glm::vec3(-50, 10, 60);
	glm::vec3 m_vec3Point14 = glm::vec3(-30, 5, 60);

	glm::vec3 m_vec3Point15 = glm::vec3(-10, 10, 60);
	glm::vec3 m_vec3Point16 = glm::vec3(10, 25, 70);
	glm::vec3 m_vec3Point17 = glm::vec3(0, 50, 80);
	glm::vec3 m_vec3Point18 = glm::vec3(-10, 25, 90);
	glm::vec3 m_vec3Point19 = glm::vec3(10, 15, 90);

	glm::vec3 m_vec3Point20 = glm::vec3(50, 15, 90);
	glm::vec3 m_vec3Point21 = glm::vec3(50, -20, 100);
	glm::vec3 m_vec3Point22 = glm::vec3(50, -25, 120);
	glm::vec3 m_vec3Point23 = glm::vec3(50, -20, 140);
	glm::vec3 m_vec3Point24 = glm::vec3(60, 0, 150);
	glm::vec3 m_vec3Point25 = glm::vec3(80, 0, 150);
	glm::vec3 m_vec3Point26 = glm::vec3(100, 2, 130);

	glm::vec3 m_vec3Point27 = glm::vec3(80, 5, 80);
	glm::vec3 m_vec3Point28 = glm::vec3(120, 0, 80);

private:

};
