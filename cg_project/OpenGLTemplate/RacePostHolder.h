#pragma once

#include "Common.h"
#include "Texture.h"
#include "VertexBufferObject.h"

// Class for generating a xz plane of a given size
class CRacePostHolder
{
public:

	/// <summary>
	/// Default constructor
	/// </summary>
	CRacePostHolder();

	/// <summary>
	/// Destructor 
	/// </summary>
	~CRacePostHolder();

	/// <summary>
	/// Create racepost object
	/// </summary>
	/// <param name="filename"></param>
	void Create(string filename);

	/// <summary>
	/// Render racepost object
	/// </summary>
	void Render();

	/// <summary>
	/// Release data of racepost object instance
	/// </summary>
	void Release();

private:

	/// <summary>
	/// VAO reference object
	/// </summary>
	GLuint m_vao;

	/// <summary>
	/// VBO Id
	/// </summary>
	UINT m_vboId;

	/// <summary>
	/// VBO Data
	/// </summary>
	vector<BYTE> m_vboData;

	/// <summary>
	/// Is data uploaded to GPU?
	/// </summary>
	bool m_isDataUploadedToGPU;

	/// <summary>
	/// Add data to VBO data object (Helper function)
	/// </summary>
	/// <param name="ptrData"></param>
	/// <param name="dataSize"></param>
	void AddDataToVBO(void* ptrData, UINT dataSize);

	/// <summary>
	/// Texture references for the object
	/// </summary>
	CTexture m_texMetal;
};
