#pragma once

#include "Common.h"
#include "Texture.h"
#include "VertexBufferObject.h"

// Class for generating a xz plane of a given size
class CRacePost
{
public:

	/// <summary>
	/// Default constructor
	/// </summary>
	CRacePost();

	/// <summary>
	/// Destructor 
	/// </summary>
	~CRacePost();

	/// <summary>
	/// Create racepost object
	/// </summary>
	/// <param name="filename"></param>
	/// <param name="a_strBoardTexture"></param>
	/// <param name="a_strFlagTexture"></param>
	/// <param name="a_fltPostHeight"></param>
	/// <param name="a_fltPostWidth"></param>
	/// <param name="a_fltTextureRepeat"></param>
	void Create(string filename, string a_strBoardTexture, string a_strFlagTexture,
		float a_fltPostHeight, float a_fltPostWidth, float a_fltTextureRepeat);
	
	/// <summary>
	/// Render racepost object
	/// </summary>
	void Render();
	
	/// <summary>
	/// Release data of racepost object instance
	/// </summary>
	void Release();

private:

	/// <summary>
	/// VAO reference object
	/// </summary>
	GLuint m_vao;

	/// <summary>
	/// VBO Id
	/// </summary>
	UINT m_vboId;									

	/// <summary>
	/// VBO Data
	/// </summary>
	vector<BYTE> m_vboData;						
	
	/// <summary>
	/// Is data uploaded to GPU?
	/// </summary>
	bool m_isDataUploadedToGPU;

	/// <summary>
	/// Add data to VBO data object (Helper function)
	/// </summary>
	/// <param name="ptrData"></param>
	/// <param name="dataSize"></param>
	void AddDataToVBO(void* ptrData, UINT dataSize);

	/// <summary>
	/// Texture references for the object
	/// </summary>
	CTexture m_texTruss;
	CTexture m_texStartBoard;
	CTexture m_textFlag;

};
