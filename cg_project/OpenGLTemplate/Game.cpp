/*
OpenGL Template for INM376 / IN3005
City University London, School of Mathematics, Computer Science and Engineering
Source code drawn from a number of sources and examples, including contributions from
 - Ben Humphrey (gametutorials.com), Michal Bubner (mbsoftworks.sk), Christophe Riccio (glm.g-truc.net)
 - Christy Quinn, Sam Kellett and others

 For educational use by Department of Computer Science, City University London UK.

 This template contains a skybox, simple terrain, camera, lighting, shaders, texturing

 Potential ways to modify the code:  Add new geometry types, shaders, change the terrain, load new meshes, change the lighting,
 different camera controls, different shaders, etc.

 Template version 5.0a 29/01/2017
 Dr Greg Slabaugh (gregory.slabaugh.1@city.ac.uk)

 version 6.0a 29/01/2019
 Dr Eddie Edwards (Philip.Edwards@city.ac.uk)

 version 6.1a 13/02/2022 - Sorted out Release mode and a few small compiler warnings
 Dr Eddie Edwards (Philip.Edwards@city.ac.uk)

*/


#include "game.h"


// Setup includes
#include "HighResolutionTimer.h"
#include "GameWindow.h"

// Game includes
#include "Camera.h"
#include "Skybox.h"
#include "Plane.h"
#include "Shaders.h"
#include "FreeTypeFont.h"
#include "Sphere.h"
#include "MatrixStack.h"
#include "OpenAssetImportMesh.h"
#include "Audio.h"
#include "Cube.h"

// Constructor
Game::Game()
{
	m_pSkybox = NULL;
	m_pCamera = NULL;
	m_pShaderPrograms = NULL;
	m_pFtFont = NULL;
	m_pBarrelMesh = NULL;
	m_pHighResolutionTimer = NULL;
	m_pAudio = NULL;
	//m_pCCube = NULL;

	m_dt = 0.0;
	m_framesPerSecond = 0;
	m_frameCount = 0;
	m_elapsedTime = 0.0f;

#pragma region COURSEWORK
	m_pRacePost = NULL;
	m_pRacePostMesh = NULL;
	m_pCRacePostHolder = NULL;
	m_lapStarted = NULL;
	m_resetControlPoint = NULL;

	//m_CCatmullRomRef = NULL;
	//m_CCatmullRomWithOffsetRef = NULL;
	m_cameraRotation = 0.0f;

	m_pFuelMeter = NULL;
	m_pScore = NULL;
	m_pVelocity = NULL;
	m_pThresholdVelocity = NULL;
	m_pNextThresholdVelocity = NULL;

	m_pTrainMesh = NULL;
	m_pCoinMesh = NULL;
	m_pNitrousMesh = NULL;
	m_pNitousDoubleMesh = NULL;

	m_CollisionDetectionManagerRef = NULL;

	m_ECameraType = ECameraType::FirstPeron;

	m_iScore = 0;
	m_fltThresholdVelocity = 50;

	m_pFBO = NULL;

#pragma endregion

}

// Destructor
Game::~Game()
{
	//game objects
	delete m_pCamera;
	delete m_pSkybox;
	delete m_pFtFont;
	delete m_pBarrelMesh;
	delete m_pAudio;
	//delete m_pCCube;

#pragma region COURSEWORK
	delete m_pRacePost;
	delete m_pRacePostMesh;
	delete m_pCRacePostHolder;
	delete m_lapStarted;
	delete m_resetControlPoint;
	delete m_CollisionDetectionManagerRef;

	delete m_pFuelMeter;
	delete m_pScore;
	delete m_pVelocity;
	delete m_pThresholdVelocity;
	delete m_pNextThresholdVelocity;

	delete m_pTrainMesh;
	delete m_pCoinMesh;
	delete m_pNitrousMesh;
	delete m_pNitousDoubleMesh;

	delete m_pFBO;
#pragma endregion

	if (m_pShaderPrograms != NULL) {
		for (unsigned int i = 0; i < m_pShaderPrograms->size(); i++)
			delete (*m_pShaderPrograms)[i];
	}
	delete m_pShaderPrograms;

	//setup objects
	delete m_pHighResolutionTimer;
}

// Initialisation:  This method only runs once at startup
void Game::Initialise()
{
	// Set the clear colour and depth
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glClearDepth(1.0f);

	/// Create objects
	m_pCamera = new CCamera;
	m_pSkybox = new CSkybox;
	m_pShaderPrograms = new vector <CShaderProgram*>;
	m_pFtFont = new CFreeTypeFont;
	m_pBarrelMesh = new COpenAssetImportMesh;
	m_pAudio = new CAudio;
	//m_pCCube = new CCube;

#pragma region COURSEWORK

	// 2D
	m_pQuad = new Quad;
	m_pEndScreen = new Quad;
	m_pFBO = new CFrameBufferObject;

	m_pRacePost = new CRacePost;
	m_pCRacePostHolder = new CRacePostHolder;
	m_pRacePostMesh = new COpenAssetImportMesh();
	m_CollisionDetectionManagerRef = new CollisionDetectionManager();

	//MESH
	m_pTrainMesh = new COpenAssetImportMesh();
	m_pCoinMesh = new COpenAssetImportMesh();
	m_pNitrousMesh = new COpenAssetImportMesh();
	m_pNitousDoubleMesh = new COpenAssetImportMesh();

	//m_pNitroMesh = new CollisionObject(new COpenAssetImportMesh());
	//m_pNitroMesh->Initialize(glm::vec3(50, 0, 0));


	m_lapStarted = new CollisionObject(new COpenAssetImportMesh());
	m_lapStarted->Initialize(glm::vec3(300, 0, 0));

	m_resetControlPoint = new CollisionObject(new COpenAssetImportMesh());
	//m_resetControlPoint->Initialize(glm::vec3(140, 0, 80));
	m_resetControlPoint->Initialize(glm::vec3(125, 0, 80));


	//m_CollisionDetectionManagerRef->Register(m_pNitroMesh);
	m_CollisionDetectionManagerRef->Register(m_lapStarted);
	m_CollisionDetectionManagerRef->Register(m_resetControlPoint);

	// HUD
	m_pFuelMeter = new CFreeTypeFont;
	m_pScore = new CFreeTypeFont;
	m_pVelocity = new CFreeTypeFont;
	m_pThresholdVelocity = new CFreeTypeFont;
	m_pNextThresholdVelocity = new CFreeTypeFont;

	// TRACK POINTS
	m_ControlPointManagerRef = new ControlPointManager;
	m_ControlPointManagerRef->Initialize(m_pCoinMesh, m_CollisionDetectionManagerRef);
	m_currentDistance = 0.0f;

	// COLLECTABLES

	m_lstNitro.push_back(new Collectable(m_ControlPointManagerRef->m_vec3Point3, m_pNitrousMesh, m_CollisionDetectionManagerRef));
	m_lstNitro.push_back(new Collectable(m_ControlPointManagerRef->m_vec3Point8, m_pNitrousMesh, m_CollisionDetectionManagerRef));
	m_lstNitro.push_back(new Collectable(m_ControlPointManagerRef->m_vec3Point15, m_pNitrousMesh, m_CollisionDetectionManagerRef));
	m_lstNitro.push_back(new Collectable(m_ControlPointManagerRef->m_vec3Point27, m_pNitrousMesh, m_CollisionDetectionManagerRef));

	m_lstNitroDouble.push_back(new Collectable(m_ControlPointManagerRef->m_vec3Point4, m_pNitousDoubleMesh, m_CollisionDetectionManagerRef));
	m_lstNitroDouble.push_back(new Collectable(m_ControlPointManagerRef->m_vec3Point11, m_pNitousDoubleMesh, m_CollisionDetectionManagerRef));
	m_lstNitroDouble.push_back(new Collectable(m_ControlPointManagerRef->m_vec3Point18, m_pNitousDoubleMesh, m_CollisionDetectionManagerRef));
	m_lstNitroDouble.push_back(new Collectable(m_ControlPointManagerRef->m_vec3Point23, m_pNitousDoubleMesh, m_CollisionDetectionManagerRef));

	// PHYSIC
	m_PhysicSystemRef = new PhysicSystem();
	m_PhysicSystemRef->Initialize(0.0f);

	m_fltThresholdVelocity = GetCurrThresholdPoint();
	m_fltNextThresholdVelocity = GetNextThresholdPoint();

#pragma endregion

	RECT dimensions = m_gameWindow.GetDimensions();

	int width = dimensions.right - dimensions.left;
	int height = dimensions.bottom - dimensions.top;

	// Set the orthographic and perspective projection matrices based on the image size
	m_pCamera->SetOrthographicProjectionMatrix(width, height);
	m_pCamera->SetPerspectiveProjectionMatrix(45.0f, (float)width / (float)height, 0.01f, 5000.0f);

	// Load shaders
	vector<CShader> shShaders;
	vector<string> sShaderFileNames;
	sShaderFileNames.push_back("mainShader.vert");
	sShaderFileNames.push_back("mainShader.frag");
	sShaderFileNames.push_back("textShader.vert");
	sShaderFileNames.push_back("textShader.frag");
	sShaderFileNames.push_back("racePostShader.vert");
	sShaderFileNames.push_back("racePostShader.frag");
	sShaderFileNames.push_back("vignetteShader.vert");
	sShaderFileNames.push_back("vignetteShader.frag");
	sShaderFileNames.push_back("explosionShader.vert"); // [NOT USED IN THE GAME]
	sShaderFileNames.push_back("explosionShader.geom"); // [NOT USED IN THE GAME]
	sShaderFileNames.push_back("explosionShader.frag"); // [NOT USED IN THE GAME]
	sShaderFileNames.push_back("fogShader.vert"); // [NOT USED IN THE GAME]
	sShaderFileNames.push_back("fogShader.frag"); // [NOT USED IN THE GAME]
	sShaderFileNames.push_back("spotlightShader.vert");
	sShaderFileNames.push_back("spotlightShader.frag");

	for (int i = 0; i < (int)sShaderFileNames.size(); i++) {
		string sExt = sShaderFileNames[i].substr((int)sShaderFileNames[i].size() - 4, 4);
		int iShaderType;
		if (sExt == "vert") iShaderType = GL_VERTEX_SHADER;
		else if (sExt == "frag") iShaderType = GL_FRAGMENT_SHADER;
		else if (sExt == "geom") iShaderType = GL_GEOMETRY_SHADER;
		else if (sExt == "tcnl") iShaderType = GL_TESS_CONTROL_SHADER;
		else iShaderType = GL_TESS_EVALUATION_SHADER;
		CShader shader;
		shader.LoadShader("resources\\shaders\\" + sShaderFileNames[i], iShaderType);
		shShaders.push_back(shader);
	}

	// Create the main shader program
	CShaderProgram* pMainProgram = new CShaderProgram;
	pMainProgram->CreateProgram();
	pMainProgram->AddShaderToProgram(&shShaders[0]);
	pMainProgram->AddShaderToProgram(&shShaders[1]);
	pMainProgram->LinkProgram();
	m_pShaderPrograms->push_back(pMainProgram);

	// Create a shader program for fonts
	CShaderProgram* pFontProgram = new CShaderProgram;
	pFontProgram->CreateProgram();
	pFontProgram->AddShaderToProgram(&shShaders[2]);
	pFontProgram->AddShaderToProgram(&shShaders[3]);
	pFontProgram->LinkProgram();
	m_pShaderPrograms->push_back(pFontProgram);

#pragma region SHADER 

	// Create a shader program for race post (basic object)
	CShaderProgram* pRacePostProgram = new CShaderProgram;
	pRacePostProgram->CreateProgram();
	pRacePostProgram->AddShaderToProgram(&shShaders[4]);
	pRacePostProgram->AddShaderToProgram(&shShaders[5]);
	pRacePostProgram->LinkProgram();
	m_pShaderPrograms->push_back(pRacePostProgram);

	// Create a shader program for race post (basic object)
	CShaderProgram* pVignetteProgram = new CShaderProgram;
	pVignetteProgram->CreateProgram();
	pVignetteProgram->AddShaderToProgram(&shShaders[6]);
	pVignetteProgram->AddShaderToProgram(&shShaders[7]);
	pVignetteProgram->LinkProgram();
	m_pShaderPrograms->push_back(pVignetteProgram);

	// Create a shader program for explosion
	CShaderProgram* pExplosionProgram = new CShaderProgram;
	pExplosionProgram->CreateProgram();
	pExplosionProgram->AddShaderToProgram(&shShaders[8]);
	pExplosionProgram->AddShaderToProgram(&shShaders[9]);
	pExplosionProgram->AddShaderToProgram(&shShaders[10]);
	pExplosionProgram->LinkProgram();
	m_pShaderPrograms->push_back(pExplosionProgram);

	// Create a shader program for fog
	CShaderProgram* pFogProgram = new CShaderProgram;
	pFogProgram->CreateProgram();
	pFogProgram->AddShaderToProgram(&shShaders[11]);
	pFogProgram->AddShaderToProgram(&shShaders[12]);
	pFogProgram->LinkProgram();
	m_pShaderPrograms->push_back(pFogProgram);

	// Create the Spotlight shader program
	CShaderProgram* pSpotlightProgram = new CShaderProgram;
	pSpotlightProgram->CreateProgram();
	pSpotlightProgram->AddShaderToProgram(&shShaders[13]);
	pSpotlightProgram->AddShaderToProgram(&shShaders[14]);
	pSpotlightProgram->LinkProgram();
	m_pShaderPrograms->push_back(pSpotlightProgram);

#pragma endregion

	// You can follow this pattern to load additional shaders

	// Create the skybox
	// Skybox downloaded from https://opengameart.org/content/interstellar-skybox 
	//m_pSkybox->Create(2500.0f);
	m_pSkybox->Create(2500.0f);

#pragma region COURSEWORK

	m_pQuad->Create("resources\\textures\\Start_tex.png", height, width);
	m_pEndScreen->Create("resources\\textures\\end_screen_bg.png", height, width);

	m_pFtFont->LoadSystemFont("arial.ttf", 32);
	m_pFtFont->SetShaderProgram(pFontProgram);

	m_pFuelMeter->LoadSystemFont("arial.ttf", 32);
	m_pFuelMeter->SetShaderProgram(pFontProgram);

	m_pScore->LoadSystemFont("arial.ttf", 32);
	m_pScore->SetShaderProgram(pFontProgram);

	m_pVelocity->LoadSystemFont("arial.ttf", 32);
	m_pVelocity->SetShaderProgram(pFontProgram);

	m_pThresholdVelocity->LoadSystemFont("arial.ttf", 32);
	m_pThresholdVelocity->SetShaderProgram(pFontProgram);

	m_pNextThresholdVelocity->LoadSystemFont("arial.ttf", 32);
	m_pNextThresholdVelocity->SetShaderProgram(pFontProgram);
	
	// Load some meshes in OBJ format
	m_pBarrelMesh->Load("resources\\models\\Barrel\\Barrel02.obj");  // Downloaded from http://www.psionicgames.com/?page_id=24 on 24 Jan 2013

	//m_pCCube->Create("resources\\textures\\smile.jpg");  // Texture downloaded from http://www.psionicgames.com/?page_id=26 on 24 Jan 2013


	/*Textures downloaded from
	i) Truss: Photo by form PxHere(https ://pxhere.com/en/photo/715191) , modified in paint3D [05/03/2022]
	ii) Flags : Image by Clker - Free - Vector Images from Pixabay(https ://pixabay.com/vectors/checkered-flag-race-sport-306237/)  [05/03/2022]
	iii) Start Board : This was created all by myself using paint3D[05 / 03 / 2022]
	*/
	m_pRacePost->Create("resources\\textures\\truss_tex.png", "resources\\textures\\Start_tex.png", "resources\\textures\\flags.png", 30.0f, 40.0f, 10.0f); // Texture downloaded from http://www.psionicgames.com/?page_id=26 on 24 Jan 2013
	m_pRacePostMesh->Load("resources\\models\\Racepost\\Racepost.obj");

	/*
	https://pxhere.com/en/photo/825220 [03/04/2022]
	*/
	m_pCRacePostHolder->Create("resources\\textures\\metal_texture.jpg");

	/*
	https://sketchfab.com/3d-models/abandonded-roller-coaster-cart-e557ecfcacb74d148ca25422d7685331#download[01/04/2022]
	*/
	m_pTrainMesh->Load("resources\\models\\Train\\train.obj");
	
	/*
	https://www.cgtrader.com/free-3d-models/character/fantasy-character/super-mario-style-golden-coin[01/04/2022]
	*/
	m_pCoinMesh->Load("resources\\models\\Coin\\coin.obj");
	
	/*
	https://www.cgtrader.com/items/2009864/download-page [01/04/2022]
	*/
	m_pNitrousMesh->Load("resources\\models\\Nitro\\nitrous_base.obj");
	
	/*
	https://www.cgtrader.com/items/875302/download-page[01/04/2022]
	*/
	m_pNitousDoubleMesh->Load("resources\\models\\Nitro2\\nitrous.obj");

#pragma endregion

	//m_pFBO->Create(width, height);
	m_iWindowHeight = height;
	m_iWindowWidth = width;

	glEnable(GL_CULL_FACE);

	// Initialise audio and play background music
	//m_pAudio->Initialise();
	//m_pAudio->LoadEventSound("resources\\Audio\\Boing.wav");					// Royalty free sound from freesound.org
	//m_pAudio->LoadMusicStream("resources\\Audio\\DST-Garote.mp3");	// Royalty free music from http://www.nosoapradio.us/
	//m_pAudio->PlayMusicStream();
}

void Game::Render()
{

#pragma region COURSEWORK

	if (m_isShowVignette)
	{
		CreateFBO();
		m_pFBO->Bind();
		RenderScene(0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		RenderScene(1);

	}
	else
	{
		RenderScene(0);

	}
#pragma endregion


	// Draw the 2D graphics after the 3D graphics
	DisplayFrameRate();

	// Swap buffers to show the rendered image
	SwapBuffers(m_gameWindow.Hdc());
}

// Render method runs repeatedly in a loop
void Game::RenderScene(int a_iPass)
{

	// Clear the buffers and enable depth testing (z-buffering)
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);


	// Set up a matrix stack
	glutil::MatrixStack modelViewMatrixStack;
	modelViewMatrixStack.SetIdentity();

	// Use the main shader program 
	CShaderProgram* pMainProgram = (*m_pShaderPrograms)[0];
	pMainProgram->UseProgram();
	pMainProgram->SetUniform("bUseTexture", true);
	pMainProgram->SetUniform("sampler0", 0);
	// Note: cubemap and non-cubemap textures should not be mixed in the same texture unit.  Setting unit 10 to be a cubemap texture.
	int cubeMapTextureUnit = 10;
	pMainProgram->SetUniform("CubeMapTex", cubeMapTextureUnit);
	// Set the projection matrix
	pMainProgram->SetUniform("matrices.projMatrix", m_pCamera->GetPerspectiveProjectionMatrix());
#pragma region COURSEWORK

#pragma region SHADER 

	// Use the race post shader program 
	CShaderProgram* pRacePostProgram = (*m_pShaderPrograms)[2];
	//pRacePostProgram->UseProgram();
	pRacePostProgram->SetUniform("bUseTexture", true);
	pRacePostProgram->SetUniform("sampler0", 0);
	int cubeMapTextureUnitForRacePost = 10;
	pRacePostProgram->SetUniform("CubeMapTex", cubeMapTextureUnitForRacePost);
	pRacePostProgram->SetUniform("matrices.projMatrix", m_pCamera->GetPerspectiveProjectionMatrix());


	// VIGNETTE SHADER PROGRAM
	CShaderProgram* pVignetteProgram = (*m_pShaderPrograms)[3];
	//pVignetteProgram->UseProgram();
	pVignetteProgram->SetUniform("bUseTexture", true);
	pVignetteProgram->SetUniform("sampler0", 0);
	int cubeMapTextureUnitForVignette = 0;
	pVignetteProgram->SetUniform("CubeMapTex", cubeMapTextureUnitForVignette);
	pVignetteProgram->SetUniform("matrices.projMatrix", m_pCamera->GetOrthographicProjectionMatrix());

	//	EXPLOSION SHADER
	CShaderProgram* pExplosionProgram = (*m_pShaderPrograms)[4];
	//pExplosionProgram->UseProgram();
	pExplosionProgram->SetUniform("bUseTexture", true);
	pExplosionProgram->SetUniform("sampler0", 0);
	int cubeMapTextureUnitForExplosion = 10;
	pExplosionProgram->SetUniform("CubeMapTex", cubeMapTextureUnitForExplosion);
	pExplosionProgram->SetUniform("matrices.projMatrix", m_pCamera->GetPerspectiveProjectionMatrix());


	CShaderProgram* pFogProgram = (*m_pShaderPrograms)[5];
	//pFogProgram->UseProgram();
	pFogProgram->SetUniform("bUseTexture", true);
	pFogProgram->SetUniform("sampler0", 0);
	int cubeMapTextureUnitForFog = 10;
	pFogProgram->SetUniform("CubeMapTex", cubeMapTextureUnitForFog);
	pFogProgram->SetUniform("matrices.projMatrix", m_pCamera->GetPerspectiveProjectionMatrix());


#pragma endregion

	// Call LookAt to create the view matrix and put this on the modelViewMatrix stack. 
	// Store the view matrix and the normal matrix associated with the view matrix for later (they're useful for lighting -- since lighting is done in eye coordinates)
	modelViewMatrixStack.LookAt(m_pCamera->GetPosition(), m_pCamera->GetView(), m_pCamera->GetUpVector());
	glm::mat4 viewMatrix = modelViewMatrixStack.Top();
	glm::mat3 viewNormalMatrix = m_pCamera->ComputeNormalMatrix(viewMatrix);


	// Set light and materials in main shader program
	glm::vec4 lightPosition1 = glm::vec4(-100, 100, -100, 1); // Position of light source *in world coordinates*
	pMainProgram->SetUniform("light1.position", viewMatrix * lightPosition1); // Position of light source *in eye coordinates*
	pMainProgram->SetUniform("light1.La", glm::vec3(1.0f));		// Ambient colour of light
	pMainProgram->SetUniform("light1.Ld", glm::vec3(1.0f));		// Diffuse colour of light
	pMainProgram->SetUniform("light1.Ls", glm::vec3(1.0f));		// Specular colour of light
	pMainProgram->SetUniform("material1.Ma", glm::vec3(1.0f));	// Ambient material reflectance
	pMainProgram->SetUniform("material1.Md", glm::vec3(0.0f));	// Diffuse material reflectance
	pMainProgram->SetUniform("material1.Ms", glm::vec3(0.0f));	// Specular material reflectance
	pMainProgram->SetUniform("material1.shininess", 15.0f);		// Shininess material property


#pragma region SHADER 
		// Set light and materials in rcae post shader program
	glm::vec4 lightPosition2 = glm::vec4(-100, 100, -100, 1); // Position of light source *in world coordinates*
	pRacePostProgram->SetUniform("light1.position", viewMatrix * lightPosition2); // Position of light source *in eye coordinates*
	pRacePostProgram->SetUniform("light1.La", glm::vec3(1.0f));		// Ambient colour of light
	pRacePostProgram->SetUniform("light1.Ld", glm::vec3(1.0f));		// Diffuse colour of light
	pRacePostProgram->SetUniform("light1.Ls", glm::vec3(1.0f));		// Specular colour of light
	pRacePostProgram->SetUniform("material1.Ma", glm::vec3(1.0f));	// Ambient material reflectance
	pRacePostProgram->SetUniform("material1.Md", glm::vec3(0.0f));	// Diffuse material reflectance
	pRacePostProgram->SetUniform("material1.Ms", glm::vec3(0.0f));	// Specular material reflectance
	pRacePostProgram->SetUniform("material1.shininess", 15.0f);		// Shininess material property

	/*
	*/
	glm::vec4 lightPosition3 = glm::vec4(-100, 100, -100, 1); // Position of light source *in world coordinates*
	pVignetteProgram->SetUniform("light1.position", viewMatrix * lightPosition3); // Position of light source *in eye coordinates*
	pVignetteProgram->SetUniform("light1.La", glm::vec3(1.0f));		// Ambient colour of light
	pVignetteProgram->SetUniform("light1.Ld", glm::vec3(1.0f));		// Diffuse colour of light
	pVignetteProgram->SetUniform("light1.Ls", glm::vec3(1.0f));		// Specular colour of light
	pVignetteProgram->SetUniform("material1.Ma", glm::vec3(1.0f));	// Ambient material reflectance
	pVignetteProgram->SetUniform("material1.Md", glm::vec3(0.0f));	// Diffuse material reflectance
	pVignetteProgram->SetUniform("material1.Ms", glm::vec3(0.0f));	// Specular material reflectance
	pVignetteProgram->SetUniform("material1.shininess", 15.0f);		// Shininess material property

	glm::vec4 lightPosition4 = glm::vec4(-100, 100, -100, 1); // Position of light source *in world coordinates*
	pExplosionProgram->SetUniform("light1.position", viewMatrix * lightPosition4); // Position of light source *in eye coordinates*
	pExplosionProgram->SetUniform("light1.La", glm::vec3(1.0f));		// Ambient colour of light
	pExplosionProgram->SetUniform("light1.Ld", glm::vec3(1.0f));		// Diffuse colour of light
	pExplosionProgram->SetUniform("light1.Ls", glm::vec3(1.0f));		// Specular colour of light
	pExplosionProgram->SetUniform("material1.Ma", glm::vec3(1.0f));	// Ambient material reflectance
	pExplosionProgram->SetUniform("material1.Md", glm::vec3(0.0f));	// Diffuse material reflectance
	pExplosionProgram->SetUniform("material1.Ms", glm::vec3(0.0f));	// Specular material reflectance
	pExplosionProgram->SetUniform("material1.shininess", 15.0f);		// Shininess material property

	glm::vec4 lightPosition5 = glm::vec4(-100, 100, -100, 1); // Position of light source *in world coordinates*
	pFogProgram->SetUniform("light1.position", viewMatrix * lightPosition5); // Position of light source *in eye coordinates*
	pFogProgram->SetUniform("light1.La", glm::vec3(1.0f));		// Ambient colour of light
	pFogProgram->SetUniform("light1.Ld", glm::vec3(1.0f));		// Diffuse colour of light
	pFogProgram->SetUniform("light1.Ls", glm::vec3(1.0f));		// Specular colour of light
	pFogProgram->SetUniform("material1.Ma", glm::vec3(1.0f));	// Ambient material reflectance
	pFogProgram->SetUniform("material1.Md", glm::vec3(0.0f));	// Diffuse material reflectance
	pFogProgram->SetUniform("material1.Ms", glm::vec3(0.0f));	// Specular material reflectance
	pFogProgram->SetUniform("material1.shininess", 15.0f);		// Shininess material property


#pragma endregion


	if (m_isShowEndScreen)
	{
		glDisable(GL_CULL_FACE);

		pMainProgram->UseProgram();
		modelViewMatrixStack.Push();
		modelViewMatrixStack.SetIdentity();
		//modelViewMatrixStack.Scale(15.0f);
		pMainProgram->SetUniform("matrices.modelViewMatrix", modelViewMatrixStack.Top());
		pMainProgram->SetUniform("matrices.projMatrix",
		m_pCamera->GetOrthographicProjectionMatrix());
		m_pEndScreen->Render(true);
		modelViewMatrixStack.Pop();
		glEnable(GL_CULL_FACE);

		return;
	}


	/*
	*/
	if (a_iPass == 1)
	{
		glDisable(GL_CULL_FACE);

		pVignetteProgram->UseProgram();
		modelViewMatrixStack.Push();
		modelViewMatrixStack.SetIdentity();
		//modelViewMatrixStack.Scale(15.0f);
		pVignetteProgram->SetUniform("matrices.modelViewMatrix", modelViewMatrixStack.Top());
		pVignetteProgram->SetUniform("matrices.projMatrix",
			m_pCamera->GetOrthographicProjectionMatrix());

		//m_pQuad->Render(); // Render quad in 2D mode
		//modelViewMatrixStack.Pop();
		m_pFBO->BindTexture(0);
		m_pQuad->Render(false);
		modelViewMatrixStack.Pop();

		glEnable(GL_CULL_FACE);
	}

	// Render the skybox and terrain with full ambient reflectance 

	pMainProgram->UseProgram();

	modelViewMatrixStack.Push();
	pMainProgram->SetUniform("renderSkybox", true);
	// Translate the modelview matrix to the camera eye point so skybox stays centred around camera
	glm::vec3 vEye = m_pCamera->GetPosition();
	modelViewMatrixStack.Translate(vEye);
	pMainProgram->SetUniform("matrices.modelViewMatrix", modelViewMatrixStack.Top());
	pMainProgram->SetUniform("matrices.normalMatrix", m_pCamera->ComputeNormalMatrix(modelViewMatrixStack.Top()));
	m_pSkybox->Render(cubeMapTextureUnit);
	pMainProgram->SetUniform("renderSkybox", false);
	modelViewMatrixStack.Pop();

	// Turn on diffuse + specular materials
	pMainProgram->SetUniform("material1.Ma", glm::vec3(0.5f));	// Ambient material reflectance
	pMainProgram->SetUniform("material1.Md", glm::vec3(0.5f));	// Diffuse material reflectance
	pMainProgram->SetUniform("material1.Ms", glm::vec3(1.0f));	// Specular material reflectance	


	// Render the racepost obj 
	pMainProgram->UseProgram();
	modelViewMatrixStack.Push();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	modelViewMatrixStack.Translate(glm::vec3(0.0f, 0.0f, -25.0f));
	//modelViewMatrixStack.Rotate(glm::vec3(0.0f, 1.0f, 0.0f), 180.0f);
	//modelViewMatrixStack.Scale(1.0f);
	pMainProgram->SetUniform("matrices.modelViewMatrix", modelViewMatrixStack.Top());
	pMainProgram->SetUniform("matrices.normalMatrix", m_pCamera->ComputeNormalMatrix(modelViewMatrixStack.Top()));
	//m_pRacePostMesh->Render();
	modelViewMatrixStack.Pop();

	if (!m_hideRacePostNHolder)
	{
		// RACE POST
		//pRacePostProgram->UseProgram();
		modelViewMatrixStack.Push();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//modelViewMatrixStack.Translate(glm::vec3(150, 0, 0));
		modelViewMatrixStack.Translate(glm::vec3(212, -0.1, 82));
		modelViewMatrixStack.Rotate(glm::vec3(0.0f, 1.0f, 0.0f), PIE_VALUE / 2); // angle in radians
		modelViewMatrixStack.Scale(0.7f);
		pRacePostProgram->SetUniform("matrices.modelViewMatrix", modelViewMatrixStack.Top());
		pRacePostProgram->SetUniform("matrices.normalMatrix", m_pCamera->ComputeNormalMatrix(modelViewMatrixStack.Top()));
		m_pRacePost->Render();
		modelViewMatrixStack.Pop();

		// RACE POST HOLDER
		modelViewMatrixStack.Push();

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//modelViewMatrixStack.Translate(glm::vec3(150, 0, 0));
		modelViewMatrixStack.Translate(glm::vec3(210, -8, 98));
		modelViewMatrixStack.Rotate(glm::vec3(0.0f, 1.0f, 0.0f), 1.5708f); // angle in radians
		modelViewMatrixStack.Scale(0.7f);
		pRacePostProgram->SetUniform("matrices.modelViewMatrix", modelViewMatrixStack.Top());
		pRacePostProgram->SetUniform("matrices.normalMatrix", m_pCamera->ComputeNormalMatrix(modelViewMatrixStack.Top()));
		m_pCRacePostHolder->Render();
		modelViewMatrixStack.Pop();
	}

	// Render the train obj 
	pMainProgram->UseProgram();

	modelViewMatrixStack.Push();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	if (m_ECameraType == ECameraType::ThirdPerson || m_ECameraType == ECameraType::TopView)
	{
		modelViewMatrixStack.Translate(m_vec3TrainPosition + glm::vec3(0, 1, 0));

		if (m_isDerail || m_isInitiateDerail)
		{
			glm::vec3 pos = glm::vec3(m_vec3TrainPosition.x, m_vec3TrainPosition.y + 100, m_vec3TrainPosition.z);
			m_pCamera->Set(pos, m_vec3TrainPosition, glm::vec3(1.0f, 0.0f, 0.0f));
		}

		if (m_isDerail)
		{
			m_fltDerailRotation += 0.005 * m_dt;
			modelViewMatrixStack.Rotate(glm::vec3(1.0f, 1.0f, 1.0f), m_fltDerailRotation);
		}
		else
		{
			modelViewMatrixStack.ApplyMatrix(PlayerRotation);
		}

		modelViewMatrixStack.Scale(0.3f);
		pMainProgram->SetUniform("matrices.modelViewMatrix", modelViewMatrixStack.Top());
		pMainProgram->SetUniform("matrices.normalMatrix", m_pCamera->ComputeNormalMatrix(modelViewMatrixStack.Top()));
		m_pTrainMesh->Render();
		modelViewMatrixStack.Pop();
	}

	pMainProgram->UseProgram();

	modelViewMatrixStack.Push();
	pMainProgram->SetUniform("bUseTexture", true); // turn on texturing
	//pMainProgram->SetUniform("bUseTexture", false); // turn off texturing
	pMainProgram->SetUniform("matrices.modelViewMatrix", modelViewMatrixStack.Top());
	pMainProgram->SetUniform("matrices.normalMatrix",
	m_pCamera->ComputeNormalMatrix(modelViewMatrixStack.Top()));
	m_ControlPointManagerRef->m_CCatmullRomRef->RenderTrack();
	modelViewMatrixStack.Pop();


	for (int i = 0; i < m_lstNitro.size(); i++)
	{
		if (!m_lstNitro[i]->IsCollided())
		{
			modelViewMatrixStack.Push();
			modelViewMatrixStack.Translate(m_lstNitro[i]->m_collsionObject->GetPosition() + glm::vec3(0, 0.5f, 0.2f));
			modelViewMatrixStack.Rotate(glm::vec3(0.0f, 1.0f, 0.0f), PIE_VALUE);
			modelViewMatrixStack.Rotate(glm::vec3(1.0f, 0.0f, 0.0f), PIE_VALUE / 4);
			modelViewMatrixStack.Rotate(glm::vec3(0.0f, 1.0f, 0.0f), m_fltCollectableRotation);
			modelViewMatrixStack.Scale(2.0f);
			pMainProgram->SetUniform("matrices.modelViewMatrix", modelViewMatrixStack.Top());
			pMainProgram->SetUniform("matrices.normalMatrix", m_pCamera->ComputeNormalMatrix(modelViewMatrixStack.Top()));
			m_lstNitro[i]->m_collsionObject->GetObjectA()->Render();
			modelViewMatrixStack.Pop();
		}
		else if (!m_lstNitro[i]->m_isFuelAdded)
		{
			m_lstNitro[i]->m_isFuelAdded = true;
			m_PhysicSystemRef->RefillFuel(FUEL_REFILL_FOR_SINGLE_NITRO);
		}
	}
	for (int i = 0; i < m_lstNitroDouble.size(); i++)
	{
		if (!m_lstNitroDouble[i]->IsCollided())
		{
			modelViewMatrixStack.Push();
			modelViewMatrixStack.Translate(m_lstNitroDouble[i]->m_collsionObject->GetPosition() + glm::vec3(0, 1.8f, -1.2f));
			modelViewMatrixStack.Rotate(glm::vec3(0.0f, 1.0f, 0.0f), PIE_VALUE);
			modelViewMatrixStack.Rotate(glm::vec3(1.0f, 0.0f, 0.0f), PIE_VALUE / 4);
			modelViewMatrixStack.Rotate(glm::vec3(0.0f, 1.0f, 0.0f), m_fltCollectableRotation);
			modelViewMatrixStack.Scale(0.4f);
			pMainProgram->SetUniform("matrices.modelViewMatrix", modelViewMatrixStack.Top());
			pMainProgram->SetUniform("matrices.normalMatrix", m_pCamera->ComputeNormalMatrix(modelViewMatrixStack.Top()));
			m_lstNitroDouble[i]->m_collsionObject->GetObjectA()->Render();
			modelViewMatrixStack.Pop();
		}
		else if (!m_lstNitroDouble[i]->m_isFuelAdded)
		{
			m_lstNitroDouble[i]->m_isFuelAdded = true;
			m_PhysicSystemRef->RefillFuel(FUEL_REFILL_FOR_DOUBLE_NITRO);
		}
	}

	if (!m_hideRacePostNHolder && m_lapStarted->IsCollided)
	{
		m_hideRacePostNHolder = true;
	}

	if (m_resetControlPoint->IsCollided)
	{
		for (int i = 0; i < m_ControlPointManagerRef->m_controlPoints.size(); i++)
		{
			if (!m_ControlPointManagerRef->m_controlPoints[i].IsCollisionObjectRegistered())
			{
				continue;
			}

			m_ControlPointManagerRef->m_controlPoints[i].ResetControlPoint();
		}

		m_fltThresholdVelocity = GetCurrThresholdPoint();
		m_fltNextThresholdVelocity = GetNextThresholdPoint();
		m_resetControlPoint->IsCollided = false;

		for (int i = 0; i < m_lstNitro.size(); i++)
		{
			m_lstNitro[i]->ResetCollectable();
		}

		for (int i = 0; i < m_lstNitroDouble.size(); i++)
		{
			m_lstNitroDouble[i]->ResetCollectable();
		}
	}

	for (int i = 0; i < m_ControlPointManagerRef->m_controlPoints.size(); i++)
	{
		if (!m_ControlPointManagerRef->m_controlPoints[i].IsCollisionObjectRegistered())
		{
			continue;
		}

		if (!m_ControlPointManagerRef->m_controlPoints[i].IsCollided())
		{
			m_fltCollectableRotation += 0.0001 * m_dt;

			modelViewMatrixStack.Push();
			modelViewMatrixStack.Translate(m_ControlPointManagerRef->m_controlPoints[i].GetPoint() + glm::vec3(0, 1.2f, 0));
			modelViewMatrixStack.Rotate(glm::vec3(0.0f, 1.0f, 0.0f), m_fltCollectableRotation);
			//modelViewMatrixStack.Scale(5 + i);
			modelViewMatrixStack.Scale(0.5f);
			pMainProgram->SetUniform("matrices.modelViewMatrix", modelViewMatrixStack.Top());
			pMainProgram->SetUniform("matrices.normalMatrix", m_pCamera->ComputeNormalMatrix(modelViewMatrixStack.Top()));
			m_ControlPointManagerRef->m_controlPoints[i].m_collsionObject->GetObjectA()->Render();
			modelViewMatrixStack.Pop();
		}
		else if (!m_ControlPointManagerRef->m_controlPoints[i].m_isScoreAdded)
		{
			m_ControlPointManagerRef->m_controlPoints[i].m_isScoreAdded = true;
			++m_iScore;
			m_fltThresholdVelocity = GetCurrThresholdPoint();
			m_fltNextThresholdVelocity = GetNextThresholdPoint();
		}
	}




	if (!m_isFadeIn && m_fltLightRotation < MAX_VALUE)
	{
		m_fltLightRotation += m_dt * 0.0001;
		m_fltReverseRotation -= m_dt * 0.0001;
	}
	else
	{
		m_isFadeIn = true;
	}

	if (m_isFadeIn && m_fltLightRotation > MIN_VALUE)
	{
		m_fltLightRotation -= m_dt * 0.0001;
		m_fltReverseRotation += m_dt * 0.0001;
	}
	else
	{
		m_isFadeIn = false;
	}

	if (!m_hideRacePostNHolder)
	{
#pragma region SHADER 

		// Switch to the Spotlight program
		CShaderProgram* pSpotlightProgram = (*m_pShaderPrograms)[6];
		pSpotlightProgram->UseProgram();

		// Set light and materials in spotlight programme
		glm::vec4 spotPosition1(165, 50, 60, 1); // Position of light source *in world coordinates*
		glm::vec4 spotPosition2(165, 50, 60, 1); // Position of light source *in world coordinates*
		pSpotlightProgram->SetUniform("light1.position", viewMatrix * spotPosition1);
		pSpotlightProgram->SetUniform("light1.La", glm::vec3(1.0f, 0.0f, 0.0f));
		pSpotlightProgram->SetUniform("light1.Ld", glm::vec3(1.0f, 0.0f, 0.0f));
		pSpotlightProgram->SetUniform("light1.Ls", glm::vec3(2.0f, 0.0f, 0.0f));
		pSpotlightProgram->SetUniform("light1.direction", glm::normalize(viewNormalMatrix * glm::vec3(0, -1, m_fltLightRotation)));
		pSpotlightProgram->SetUniform("light1.exponent", 5.0f);
		pSpotlightProgram->SetUniform("light1.cutoff", 10.0f);
		pSpotlightProgram->SetUniform("light2.position", viewMatrix * spotPosition2);
		pSpotlightProgram->SetUniform("light2.La", glm::vec3(0.0f, 1.0f, 0.0f));
		pSpotlightProgram->SetUniform("light2.Ld", glm::vec3(0.0f, 1.0f, 0.0f));
		pSpotlightProgram->SetUniform("light2.Ls", glm::vec3(0.0f, 1.0f, 0.0f));
		pSpotlightProgram->SetUniform("light2.direction", glm::normalize(viewNormalMatrix * glm::vec3(0, -1, m_fltReverseRotation)));
		pSpotlightProgram->SetUniform("light2.exponent", 5.0f);
		pSpotlightProgram->SetUniform("light2.cutoff", 10.0f);
		pSpotlightProgram->SetUniform("material1.shininess", 15.0f);
		pSpotlightProgram->SetUniform("material1.Ma", glm::vec3(0.0f, 0.0f, 0.2f));
		pSpotlightProgram->SetUniform("material1.Md", glm::vec3(0.0f, 0.0f, 1.0f));
		pSpotlightProgram->SetUniform("material1.Ms", glm::vec3(1.0f, 1.0f, 1.0f));

		pSpotlightProgram->UseProgram();
#pragma endregion

		for (int z = -5; z <= 5; z++) {
			for (int x = -5; x <= 5; x++) {

				modelViewMatrixStack.Push();
				glEnable(GL_BLEND);
				modelViewMatrixStack.Translate(glm::vec3(170 + x * 2, -5, 80 + z * 2));
				modelViewMatrixStack.Scale(3.0f);
				pSpotlightProgram->SetUniform("matrices.projMatrix", m_pCamera->GetPerspectiveProjectionMatrix());
				pSpotlightProgram->SetUniform("matrices.modelViewMatrix", modelViewMatrixStack.Top());
				pSpotlightProgram->SetUniform("matrices.normalMatrix", m_pCamera->ComputeNormalMatrix(modelViewMatrixStack.Top()));
				m_pBarrelMesh->Render();
				modelViewMatrixStack.Pop();

			}
		}
	}

#pragma endregion
	// Draw the 2D graphics after the 3D graphics
	//DisplayFrameRate();

	// Swap buffers to show the rendered image
	//SwapBuffers(m_gameWindow.Hdc());

}

// Update method runs repeatedly with the Render method
void Game::Update()
{
	// Update the camera using the amount of time that has elapsed to avoid framerate dependent motion
	m_pCamera->Update(m_dt);

	m_pAudio->Update();

#pragma region COURSEWORK

	// ************* END SCREEN  **************

	if (m_isShowEndScreen)
	{
		return;
	}

	if (m_isDerail)
	{
		m_vec3LastTrainPosition = m_vec3TrainPosition;
		m_vec3FallPosition = glm::vec3(0.01 * m_dt, 0.01 * m_dt, 0.01 * m_dt);
		m_vec3TrainPosition -= m_vec3FallPosition;

		m_fltTimerAfterDerial += 0.00036f * m_dt;

		if (m_fltTimerAfterDerial > 3.0f)
		{
			m_isShowEndScreen = true;
		}

		return;
	}

	// ************* PLAYER MOVEMENT AND CAMERA ANGLES**************
	//m_currentDistance += m_dt * 0.1f * m_speed;
	m_currentDistance += m_dt * 0.1f * m_PhysicSystemRef->getSpeed();

	glm::vec3 p;
	m_ControlPointManagerRef->m_CCatmullRomRef->Sample(m_currentDistance, p);

	float l_fltDistanceAhead = m_currentDistance + 1.0f;
	glm::vec3 pNext;
	m_ControlPointManagerRef->m_CCatmullRomRef->Sample(l_fltDistanceAhead, pNext);

	glm::vec3 T = glm::normalize(pNext - p);

	glm::vec3 N = glm::normalize(glm::cross(T, glm::vec3(0, 1, 0)));
	glm::vec3 B = glm::normalize(glm::cross(N, T));

	glm::vec3 up = glm::rotate(glm::vec3(0, 1, 0), m_cameraRotation, T);


	glm::vec3 p_with_offset;
	m_ControlPointManagerRef->m_CCatmullRomWithOffsetRef->Sample(m_currentDistance, p_with_offset);

	float l_fltDistanceAhead_with_offset = m_currentDistance + 1.0f;
	glm::vec3 pNext_with_offset;
	m_ControlPointManagerRef->m_CCatmullRomWithOffsetRef->Sample(l_fltDistanceAhead_with_offset, pNext_with_offset);

	glm::vec3 T_offset = glm::normalize(pNext_with_offset - p_with_offset);
	glm::vec3 up_offset = glm::rotate(glm::vec3(0, 1, 0), m_cameraRotation, T_offset);
	glm::vec3 N_offset = glm::normalize(glm::cross(T_offset, up_offset));
	glm::vec3 B_offset = glm::normalize(glm::cross(N_offset, T_offset));

	glm::vec3 NormalizedT = glm::normalize(T);

	m_PhysicSystemRef->CalculateGravity(NormalizedT.y);
	m_PhysicSystemRef->Update(m_dt);

	m_vec3TrainPosition = p + B;
	PlayerRotation = glm::mat4(glm::mat3(N, B, -T));

	if (!m_isTesting)
	{
		glm::vec3 l_temp;

		//glm::vec3 pos;

		switch (m_ECameraType)
		{
		case ECameraType::FirstPeron:
			m_pCamera->Set(p + B, p + 10.0f * T, up);
			//m_pCamera->Set(p + B, p + 10.0f * T, up);
			break;

		case ECameraType::ThirdPerson:
			l_temp = p_with_offset + B_offset;
			m_pCamera->Set(l_temp, p_with_offset + 5.0f * T_offset, up_offset);
			
			break;

		case ECameraType::TopView:
			//m_pCamera->Set(pNext, p + 10.0f * T, up);

			/*pos = glm::vec3(m_vec3TrainPosition.x, m_vec3TrainPosition.y + 100, m_vec3TrainPosition.z);
			m_pCamera->Set(pos, m_vec3TrainPosition, glm::vec3(1.0f, 0.0f, 0.0f));*/
			break;

		default:
			// Type not found warning message

			break;
		}

	}

	m_CollisionDetectionManagerRef->Update(m_vec3TrainPosition);


	// ***************** DERAIL ***************
	if (m_fltThresholdVelocity < m_fltVelocityDisplay)
	{
		m_isInitiateDerail = true;
		//Derail();
	}

	if (m_isInitiateDerail)
	{
		m_isShowVignette = true;
		m_fltTimerBeforeDerial += 0.00036f* m_dt;
		m_ECameraType = ECameraType::TopView;
	}

	if (m_fltTimerBeforeDerial > 1.0f)
	{
		m_isInitiateDerail = false;
		Derail();
	}

	// **********  END GAME IF NO FUEL AND NO VELOCITY ************
	if (m_fltVelocityDisplay < 0.001f && m_PhysicSystemRef->Fuel < 0.1f)
	{
		m_timerForNoFuelNoVelocity += 0.00036f * m_dt;
	}

	if (m_timerForNoFuelNoVelocity > 1.5f)
	{
		m_isShowEndScreen = true;
	}
	
	// ******************* CAMERA SHAKE (ADVANCED TECHNEQUE) **************
	if (m_fltThresholdVelocity - m_fltVelocityDisplay < THRESHOLD_DIFFERENCE_FOR_CAMERA_SHAKE)
	{
		StartCameraShake();
	}
	else
	{
		StopCameraShake();
	}

#pragma endregion
}

void Game::DisplayFrameRate()
{
	CShaderProgram* fontProgram = (*m_pShaderPrograms)[1];

	RECT dimensions = m_gameWindow.GetDimensions();
	int height = dimensions.bottom - dimensions.top;

	int width = dimensions.right - dimensions.left;

	// Increase the elapsed time and frame counter
	m_elapsedTime += m_dt;
	m_frameCount++;

	// Now we want to subtract the current time by the last time that was stored
	// to see if the time elapsed has been over a second, which means we found our FPS.
	if (m_elapsedTime > 1000)
	{
		m_elapsedTime = 0;
		m_framesPerSecond = m_frameCount;

		// Reset the frames per second
		m_frameCount = 0;
	}

#pragma region COURSEWORK

	if (m_framesPerSecond > 0) {
		// Use the font shader program and render the text
		fontProgram->UseProgram();
		glDisable(GL_DEPTH_TEST);
		fontProgram->SetUniform("matrices.modelViewMatrix", glm::mat4(1));
		fontProgram->SetUniform("matrices.projMatrix", m_pCamera->GetOrthographicProjectionMatrix());
		fontProgram->SetUniform("vColour", glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
		m_pFtFont->Render(width - 100, 20, 20, "FPS: %i", m_framesPerSecond);

		if (m_isShowEndScreen)
		{
			m_pScore->Render(width/3, height/2, 35, "GAME OVER!! \n\nYOUR SCORE: %i", m_iScore);
		}
		
		if (!m_isDerail && !m_isInitiateDerail && !m_isShowEndScreen)
		{
			/// TO SHOW ONLY 2 DIGITS AFTER DECIMAL
			float l_fltVelocityTemp = m_PhysicSystemRef->getSpeed() * 100;
			int l_iVelocityTemp = (int)l_fltVelocityTemp;
			m_fltVelocityDisplay = (float)l_iVelocityTemp / 100;

			int l_iVelocityDisplayOnHud = (int)(m_fltVelocityDisplay * 100);
			int l_iThresholdDisplayOnHud = (int)(m_fltThresholdVelocity * 100);
			int l_iNextThresholdDisplayOnHud = (int)(m_fltNextThresholdVelocity * 100);

			fontProgram->SetUniform("vColour", glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
			m_pFuelMeter->Render(width - 100, height - 40, 20, "FUEL: %i", (int)m_PhysicSystemRef->Fuel);

			fontProgram->SetUniform("vColour", glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
			m_pScore->Render(20, height - 40, 18, "SCORE: %i", m_iScore);
			m_pVelocity->Render(20, height - 70, 18, "VELOCITY: %i", l_iVelocityDisplayOnHud);
			m_pThresholdVelocity->Render(20, height - 100, 18, "CURRENT THRESHOLD: %i", l_iThresholdDisplayOnHud);
			m_pNextThresholdVelocity->Render(20, height - 130, 18, "NEXT THRESHOLD: %i", l_iNextThresholdDisplayOnHud);
			
			/*m_pVelocity->Render(20, height - 70, 20, "VELOCITY: %.2lf", m_fltVelocityDisplay);
			m_pThresholdVelocity->Render(20, height - 100, 20, "THRESHOLD VELOCITY: %.2lf", m_fltThresholdVelocity);*/
		}

	}

#pragma endregion

}

// The game loop runs repeatedly until game over
void Game::GameLoop()
{
	/*
	// Fixed timer
	dDt = pHighResolutionTimer->Elapsed();
	if (dDt > 1000.0 / (double) Game::FPS) {
		pHighResolutionTimer->Start();
		Update();
		Render();
	}
	*/


	// Variable timer
	m_pHighResolutionTimer->Start();
	Update();
	Render();
	m_dt = m_pHighResolutionTimer->Elapsed();


}


WPARAM Game::Execute()
{
	m_pHighResolutionTimer = new CHighResolutionTimer;
	m_gameWindow.Init(m_hInstance);

	if (!m_gameWindow.Hdc()) {
		return 1;
	}

	Initialise();

	m_pHighResolutionTimer->Start();


	MSG msg;

	while (1) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				break;
			}

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else if (m_appActive) {
			GameLoop();
		}
		else Sleep(200); // Do not consume processor power if application isn't active
	}

	m_gameWindow.Deinit();

	return(msg.wParam);
}

LRESULT Game::ProcessEvents(HWND window, UINT message, WPARAM w_param, LPARAM l_param)
{
	LRESULT result = 0;

	switch (message) {


	case WM_ACTIVATE:
	{
		switch (LOWORD(w_param))
		{
		case WA_ACTIVE:
		case WA_CLICKACTIVE:
			m_appActive = true;
			m_pHighResolutionTimer->Start();
			break;
		case WA_INACTIVE:
			m_appActive = false;
			break;
		}
		break;
	}

	case WM_SIZE:
		RECT dimensions;
		GetClientRect(window, &dimensions);
		m_gameWindow.SetDimensions(dimensions);
		break;

	case WM_PAINT:
		PAINTSTRUCT ps;
		BeginPaint(window, &ps);
		EndPaint(window, &ps);
		break;

	case WM_KEYDOWN:
		switch (w_param) {
		case VK_ESCAPE:
			PostQuitMessage(0);
			break;
#pragma region COURSEWORK

		case '1':
			//Derail();
			break;
		case '2':

			break;
		case '3':
			break;
		case '4':
			break;
		case VK_F1:
			break;
		case VK_RIGHT:
			break;
		case VK_LEFT:
			break;

		case VK_SPACE:
		
			if (m_isInitiateDerail || m_isDerail)
			{
				break;
			}

			m_PhysicSystemRef->StartAcceleration();
			break;

		case VK_CONTROL:
			
			if (m_isInitiateDerail || m_isDerail)
			{
				break;
			}
			m_PhysicSystemRef->StartBoost();
			break;

		case VK_SHIFT:
			m_isTesting = !m_isTesting;    // SHORTCUT TESTING KEY
			break;

		case VK_TAB:
			m_PhysicSystemRef->RefillFuel(10); 
			break;

		//case VK_BACK:
		case VK_RETURN:
			
			if (m_isInitiateDerail || m_isDerail)
			{
				break;
			}

			if (m_ECameraType == ECameraType::FirstPeron)
			{
				m_ECameraType = ECameraType::ThirdPerson;
			}
			else
			{
				m_ECameraType = ECameraType::FirstPeron;
			}

			break;
		}
		break;

	case WM_KEYUP:
		switch (w_param) {

		case VK_SPACE:
			m_PhysicSystemRef->StopAcceleration();
			break;

		case VK_CONTROL:
			m_PhysicSystemRef->StopBoost();
			break;
		}

		break;
#pragma endregion

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		result = DefWindowProc(window, message, w_param, l_param);
		break;
	}

	return result;
}

Game& Game::GetInstance()
{
	static Game instance;

	return instance;
}

void Game::SetHinstance(HINSTANCE hinstance)
{
	m_hInstance = hinstance;
}

LRESULT CALLBACK WinProc(HWND window, UINT message, WPARAM w_param, LPARAM l_param)
{
	return Game::GetInstance().ProcessEvents(window, message, w_param, l_param);
}

int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE, PSTR, int)
{
	Game& game = Game::GetInstance();
	game.SetHinstance(hinstance);

	return int(game.Execute());
}

#pragma region COURSEWORK

/// <summary>
/// Derial train
/// </summary>
void Game::Derail()
{
	// rotate traim using rotate X
	m_isDerail = true;
	m_ECameraType = ECameraType::TopView;
}

/// <summary>
/// Get current threshold velocity
/// </summary>
float Game::GetCurrThresholdPoint()
{
	float l_fltFirstControlPoint = 0.0f;
	bool l_isRegistered = false;
	for (int i = 0; i < m_ControlPointManagerRef->m_controlPoints.size(); i++)
	{
		if (!m_ControlPointManagerRef->m_controlPoints[i].IsCollisionObjectRegistered())
		{
			continue;
		}

		if (!l_isRegistered && m_ControlPointManagerRef->m_controlPoints[i].IsCollided())
		{
			l_fltFirstControlPoint = m_ControlPointManagerRef->m_controlPoints[i].GetThreshold();
			l_isRegistered = true;
		}

		if (!m_ControlPointManagerRef->m_controlPoints[i].IsCollided())
		{
			return m_ControlPointManagerRef->m_controlPoints[i].GetThreshold();
		}
	}

	return l_fltFirstControlPoint;
}

/// <summary>
/// get next points threshold velocity
/// </summary>
float Game::GetNextThresholdPoint()
{
	float l_fltFirstControlPoint = 0.0f;
	bool l_isRegistered = false;
	bool l_isFirstMiss = false;

	for (int i = 0; i < m_ControlPointManagerRef->m_controlPoints.size(); i++)
	{
		if (!m_ControlPointManagerRef->m_controlPoints[i].IsCollisionObjectRegistered())
		{
			continue;
		}

		if (!l_isRegistered && m_ControlPointManagerRef->m_controlPoints[i].IsCollided())
		{
			l_fltFirstControlPoint = m_ControlPointManagerRef->m_controlPoints[i].GetThreshold();
			l_isRegistered = true;
		}

		if (!m_ControlPointManagerRef->m_controlPoints[i].IsCollided())
		{
			if (!l_isFirstMiss)
			{
				l_isFirstMiss = true;
				continue;
			}
			return m_ControlPointManagerRef->m_controlPoints[i].GetThreshold();
		}
	}

	return l_fltFirstControlPoint;

}

/// <summary>
/// Create FBO
/// </summary>
void Game::CreateFBO()
{
	if (m_isFBOCreated)
	{
		return;
	}
	m_pFBO->Create(m_iWindowWidth, m_iWindowHeight);
	m_isFBOCreated = true;
}

/// <summary>
/// Start Camera shake
/// </summary>
void Game::StartCameraShake()
{

	if (!m_isFirstCycleRotation && m_cameraRotation < ROTATION_MAX_VALUE)
	{
		m_cameraRotation += m_dt * 0.0005f;
	}
	else
	{
		m_isFirstCycleRotation = true;
	}

	if (m_isFirstCycleRotation && m_cameraRotation > ROTATION_MIN_VALUE)
	{
		m_cameraRotation -= m_dt * 0.0005f;
	}
	else
	{
		m_isFirstCycleRotation = false;
	}

}

/// <summary>
/// Stop camera shake
/// </summary>
void Game::StopCameraShake()
{
	m_cameraRotation = 0;
}


#pragma endregion

