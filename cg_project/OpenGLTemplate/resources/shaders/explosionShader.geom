﻿#version 400 core


uniform bool bExplodeObject;
uniform float explodeFactor;

 out vec3 vColour;
 out vec2 vTexCoord;

in vec3 vColourPass[];
in vec2 vTexCoordPass[];

in	mat4 projMatrix[];
in	mat4 modelViewMatrix[];


//layout (lines​) in; 
//layout (line_strip​, max_vertices = 10​) out;

layout (triangles) in;
layout (line_strip, max_vertices = 6) out;


void main() {
float localExplode = bExplodeObject? explodeFactor: 0.0;
// Get triangle normal
vec3 e1 = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;
vec3 e2 = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz;
vec3 n = normalize(cross(e1, e2));
for (int i = 0; i < 3; i++) {
vec4 explodedPos = gl_in[i].gl_Position + localExplode * vec4(n, 0);
gl_Position = projMatrix[0] * modelViewMatrix[0] * explodedPos;
vColour = vColourPass[i];
vTexCoord = vTexCoordPass[i];
EmitVertex();
}
EndPrimitive();
}