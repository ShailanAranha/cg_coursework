#version 400 core

in vec3 vColour;			// Interpolated colour using colour calculated in the vertex shader
in vec2 vTexCoord;			// Interpolated texture coordinate using texture coordinate from the vertex shader

out vec4 vOutputColour;		

uniform sampler2D sampler0;  
uniform samplerCube CubeMapTex;
uniform bool bUseTexture;   
uniform bool renderSkybox;
in vec3 worldPosition;


void main()
{

/*
	if (renderSkybox) {
		vOutputColour = texture(CubeMapTex, worldPosition);

	} else {

		// Get the texel colour from the texture sampler
		vec4 vTexColour = texture(sampler0, vTexCoord);	

		if (bUseTexture)
			vOutputColour = vTexColour*vec4(vColour, 1.0f);	// Combine object colour and texture 
		else
			vOutputColour = vec4(vColour, 1.0f);	// Just use the colour instead
	}

vec4 texColour = texture(sampler0, vTexCoord);
vec2 vectorFromCentre = vTexCoord.xy - vec2(0.5);
float vignette = 1 - smoothstep(0.3, 0.75,
length(vectorFromCentre));
texColour.rgb *= vignette;
vOutputColour = texColour;
	

	if (renderSkybox) {
		vOutputColour = texture(CubeMapTex, worldPosition);

	} else {
*/

vec4 texColour = texture(sampler0, vTexCoord);
vec2 vectorFromCentre = vTexCoord.xy - vec2(0.5);
float vignette = 1 - smoothstep(0.3, 0.75,
length(vectorFromCentre));
texColour.rgb *= vignette;
vOutputColour = texColour;

//}

}
