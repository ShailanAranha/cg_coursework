#include "Collectable.h"

/// <summary>
/// Default contructor
/// </summary>
/// <param name="a_vec3Point"></param>
/// <param name="a_mesh"></param>
/// <param name="m_CollisionDetectionManagerRef"></param>
Collectable::Collectable(glm::vec3 a_vec3Point, COpenAssetImportMesh* a_mesh, CollisionDetectionManager* m_CollisionDetectionManagerRef): m_vec3Point(a_vec3Point), m_collsionObject ( new CollisionObject(a_mesh))
{
	m_collsionObject->Initialize(m_vec3Point);
	m_CollisionDetectionManagerRef->Register(m_collsionObject);
}

/// <summary>
/// Destructor
/// </summary>
Collectable::~Collectable()
{}

/// <summary>
/// Get position 
/// </summary>
/// <returns></returns>
glm::vec3 Collectable::GetPoint()
{
	return m_vec3Point;
}

/// <summary>
/// Is collectable collected?
/// </summary>
/// <returns></returns>
bool Collectable::IsCollided()
{
	return  m_collsionObject->IsCollided;
}

/// <summary>
/// Reset collectable dtat to de4fault value (Non collected state)
/// </summary>
void Collectable::ResetCollectable()
{
	m_isFuelAdded = false;
	m_collsionObject->ResetCollision();
}

