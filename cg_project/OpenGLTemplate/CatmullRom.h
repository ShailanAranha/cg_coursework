#pragma once
#include "Common.h"
#include "vertexBufferObject.h"
#include "vertexBufferObjectIndexed.h"
#include "Texture.h"


class CCatmullRom
{
public:

	/// <summary>
	/// Default contructor
	/// </summary>
	CCatmullRom();
	~CCatmullRom();

	/// <summary>
	/// Create centreline of spline
	/// </summary>
	/// <param name="a_controlPoints"></param>
	void CreateCentreline(const vector<glm::vec3>& a_controlPoints);
	
	/// <summary>
	/// Render centreline
	/// </summary>
	void RenderCentreline();

	/// <summary>
	/// Create offset curves
	/// </summary>
	/// <param name="a_vector"></param>
	void CreateOffsetCurves(glm::vec3 a_vector);

	/// <summary>
	/// Render offset curves
	/// </summary>
	void RenderOffsetCurves();

	/// <summary>
	/// Create track
	/// </summary>
	/// <param name="a_strTexture"></param>
	void CreateTrack(string a_strTexture);

	/// <summary>
	/// Render track
	/// </summary>
	void RenderTrack();

	/// <summary>
	/// Return the currvent lap (starting from 0) based on distance along the control curve.
	/// </summary>
	/// <param name="d"></param>
	/// <returns></returns>
	int CurrentLap(float d); 

	/// <summary>
	///  Return a point on the centreline based on a certain distance along the control curve.
	/// </summary>
	/// <param name="d"></param>
	/// <param name="p"></param>
	/// <param name="up"></param>
	/// <returns></returns>
	bool Sample(float d, glm::vec3 &p, glm::vec3 &up = _dummy_vector); 
	
	/// <summary>
	/// Sample with offset value
	/// </summary>
	/// <param name="d"></param>
	/// <param name="p"></param>
	/// <param name="up"></param>
	/// <returns></returns>
	bool SampleWithOffset(float d, glm::vec3& p, glm::vec3& up = _dummy_vector);

private:
	/// <summary>
	/// Set control points
	/// </summary>
	/// <param name="a_controlPoints"></param>
	void SetControlPoints(const vector<glm::vec3>& a_controlPoints);

	/// <summary>
	///Determine lengths along the control points, which is the set of control points forming the closed curve 
	/// </summary>
	void ComputeLengthsAlongControlPoints();

	/// <summary>
	/// Sample a set of control points using an open Catmull-Rom spline, to produce a set of iNumSamples that are (roughly) equally spaced
	/// </summary>
	/// <param name="numSamples"></param>
	void UniformlySampleControlPoints(int numSamples);

	/// <summary>
	///  Perform Catmull Rom spline interpolation between four points, interpolating the space between p1 and p2
	/// </summary>
	/// <param name="p0"></param>
	/// <param name="p1"></param>
	/// <param name="p2"></param>
	/// <param name="p3"></param>
	/// <param name="t"></param>
	/// <returns></returns>
	glm::vec3 Interpolate(glm::vec3 &p0, glm::vec3 &p1, glm::vec3 &p2, glm::vec3 &p3, float t);
	
	/// <summary>
	/// Control values with offset  (NOT USED)
	/// </summary>
	vector<glm::vec3> m_controlPoints_with_offset;		

	/// <summary>
	/// Control up vectors values with offset  (NOT USED)
	/// </summary>
	vector<glm::vec3> m_controlUpVectors_with_offset;

	/// <summary>
	/// Offset value for offset samples
	/// </summary>
	glm::vec3 m_offsetValue = glm::vec3(0, 2, 0);
	
	/// <summary>
	/// Distances
	/// </summary>
	vector<float> m_distances;

	/// <summary>
	/// Texture
	/// </summary>
	CTexture m_texture;

	/// <summary>
	/// Centreline VAO
	/// </summary>
	GLuint m_vaoCentreline;

	/// <summary>
	/// Left Offset Curve VAO
	/// </summary>
	GLuint m_vaoLeftOffsetCurve;

	/// <summary>
	/// Right Offset Curve VAO
	/// </summary>
	GLuint m_vaoRightOffsetCurve;

	/// <summary>
	/// Vao for Track
	/// </summary>
	GLuint m_vaoTrack;

	/// <summary>
	/// Dummy vector
	/// </summary>
	static glm::vec3 _dummy_vector;
	
	/// <summary>
	/// Control points, which are interpolated to produce the centreline points
	/// </summary>
	vector<glm::vec3> m_controlPoints;		

	/// <summary>
	/// Control upvectors, which are interpolated to produce the centreline upvectors
	/// </summary>
	vector<glm::vec3> m_controlUpVectors;	
	
	/// <summary>
	/// Centreline points
	/// </summary>
	vector<glm::vec3> m_centrelinePoints;

	/// <summary>
	/// Centreline upvectors
	/// </summary>
	vector<glm::vec3> m_centrelineUpVectors;

	/// <summary>
	///  Left offset curve points
	/// </summary>
	vector<glm::vec3> m_leftOffsetPoints;

	/// <summary>
	/// Right offset curve points
	/// </summary>
	vector<glm::vec3> m_rightOffsetPoints;

	/// <summary>
	/// Number of vertices in the track VBO
	/// </summary>
	unsigned int m_vertexCount;				

	/// <summary>
	/// VAO reference object
	/// </summary>
	GLuint m_vao;

	/// <summary>
	/// VAO Merge reference object
	/// </summary>
	GLuint m_vaoMerge;

	/// <summary>
	/// Roller coaster track texture
	/// </summary>
	CTexture m_texRollarCoasterTrack;

};
