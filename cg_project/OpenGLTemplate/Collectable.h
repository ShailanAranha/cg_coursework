#pragma once
#include "OpenAssetImportMesh.h"
#include "Common.h"
#include "CollisionObject.h"
#include "CollisionDetectionManager.h"

class Collectable
{
private:

	/// <summary>
	/// Collectable ID
	/// </summary>
	std::string m_strId = "";

	/// <summary>
	/// Collectable position
	/// </summary>
	glm::vec3 m_vec3Point;

public:

	/// <summary>
	/// Default contructor
	/// </summary>
	/// <param name="a_vec3Point"></param>
	/// <param name="a_mesh"></param>
	/// <param name="m_CollisionDetectionManagerRef"></param>
	Collectable(glm::vec3 a_vec3Point, COpenAssetImportMesh* a_mesh, CollisionDetectionManager* m_CollisionDetectionManagerRef);
	
	/// <summary>
	/// Destructor
	/// </summary>
	~Collectable();

	/// <summary>
	/// Get position 
	/// </summary>
	/// <returns></returns>
	glm::vec3 GetPoint();

	/// <summary>
	/// Collection object instance
	/// </summary>
	CollisionObject* m_collsionObject;

	/// <summary>
	/// Is collectable collected?
	/// </summary>
	/// <returns></returns>
	bool IsCollided();

	/// <summary>
	/// Is fuel added?
	/// </summary>
	bool m_isFuelAdded = false;

	/// <summary>
	/// Reset collectable dtat to de4fault value (Non collected state)
	/// </summary>
	void ResetCollectable();

};
