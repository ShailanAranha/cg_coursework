#pragma once
#include <vector>
#include "Common.h"
#include "CollisionObject.h"

class CollisionObject;

class CollisionDetectionManager
{
private:

	/// <summary>
	/// List of collidable objects
	/// </summary>
	std::vector<CollisionObject*> m_lstCollidables;

	/// <summary>
	/// Threshold value between player to object after which collsion is detected
	/// </summary>
	float m_fltThreshold = 3.0f;

public:

	/// <summary>
	/// Default constructor
	/// </summary>
	CollisionDetectionManager();

	/// <summary>
	/// Destructor
	/// </summary>
	~CollisionDetectionManager();

	/// <summary>
	/// Register colliable object to the list to detect collision
	/// </summary>
	/// <param name="a_CollisionObject"></param>
	void Register(CollisionObject* a_CollisionObject);

	/// <summary>
	/// Game loop, check the distance between each collidable object regitered and the player position and triggers collision if the value is less than threshold
	/// </summary>
	/// <param name="a_playerPosition"></param>
	void Update(glm::vec3 a_playerPosition);

	/// <summary>
	/// Change the threshold for collision
	/// </summary>
	/// <param name="a_fltThreshold"></param>
	void ChangeThreshold(float a_fltThreshold);

	/// <summary>
	/// For testing
	/// </summary>
	float m_fltTest = 0.0f;
};

