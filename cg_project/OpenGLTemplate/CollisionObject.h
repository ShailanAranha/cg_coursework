#pragma once
#include "OpenAssetImportMesh.h"
#include "Common.h"


class CollisionObject
{

private:

	/// <summary>
	/// Position of the object
	/// </summary>
	glm::vec3 m_position;

	/// <summary>
	/// Object instance
	/// </summary>
	COpenAssetImportMesh* m_object;

public:

	/// <summary>
	/// Parameterized contructor
	/// </summary>
	/// <param name="a_object"></param>
	CollisionObject(COpenAssetImportMesh* a_object);
	CollisionObject(COpenAssetImportMesh* a_object, glm::vec3 a_position);
	~CollisionObject();

	/// <summary>
	/// Initaltate object with data
	/// </summary>
	/// <param name="a_position"></param>
	void Initialize( glm::vec3 a_position);

	/// <summary>
	/// Get position of the object (Read-Only)
	/// </summary>
	/// <returns></returns>
	glm::vec3 GetPosition();

	/// <summary>
	/// Get instance of the object (Read-Only)
	/// </summary>
	/// <returns></returns>
	COpenAssetImportMesh* GetObject();

	/// <summary>
	/// Is object collied with the player?
	/// </summary>
	bool IsCollided = false;

	/// <summary>
	/// Reset collision info (Reset value to non-collsion state)
	/// </summary>
	void ResetCollision();

};