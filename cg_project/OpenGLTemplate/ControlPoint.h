#pragma once
#include "Common.h"
#include "CollisionObject.h"
#include "CollisionDetectionManager.h"


class ControlPoint
{
private:

	/// <summary>
	/// Control point Id
	/// </summary>
	std::string m_strId ="";

	/// <summary>
	/// Control point value
	/// </summary>
	glm::vec3 m_vec3Point;

	/// <summary>
	/// Derail threshold at that point
	/// </summary>
	float m_fltThreshold;

	/// <summary>
	/// Is control point register for any collsion detetcion?
	/// </summary>
	bool m_isCollisionObjectRegistered = false;

public:

	/// <summary>
	/// Contructor
	/// </summary>
	/// <param name="a_vec3Point"></param>
	/// <param name="a_fltThreshold"></param>
	ControlPoint(glm::vec3 a_vec3Point, float a_fltThreshold);
	ControlPoint(glm::vec3 a_vec3Point, float a_fltThreshold, COpenAssetImportMesh* a_mesh, CollisionDetectionManager* m_CollisionDetectionManagerRef);
	~ControlPoint();

	/// <summary>
	/// Get control point
	/// </summary>
	/// <returns></returns>
	glm::vec3 GetPoint();

	/// <summary>
	/// Get derail threshold value st the point
	/// </summary>
	/// <returns></returns>
	float GetThreshold();

	/// <summary>
	/// Collsion object (Optional)
	/// </summary>
	CollisionObject* m_collsionObject;

	/// <summary>
	/// Is control point register for any collsion detetcion? (Read-only)
	/// </summary>
	/// <returns></returns>
	bool IsCollisionObjectRegistered();

	/// <summary>
	/// Is object at control point collided?
	/// </summary>
	/// <returns></returns>
	bool IsCollided();

	/// <summary>
	/// Is score added?
	/// </summary>
	bool m_isScoreAdded = false;

	/// <summary>
	/// Reset all properties of control point
	/// </summary>
	void ResetControlPoint();

};