#include "ControlPoint.h"


/// <summary>
/// Contructor
/// </summary>
/// <param name="a_vec3Point"></param>
/// <param name="a_fltThreshold"></param>
ControlPoint::ControlPoint(glm::vec3 a_vec3Point, float a_fltThreshold)
{
	m_vec3Point = a_vec3Point;
	m_fltThreshold = a_fltThreshold;
	//m_collsionObject = new CollisionObject();
}

ControlPoint::ControlPoint(glm::vec3 a_vec3Point, float a_fltThreshold, COpenAssetImportMesh* a_mesh, CollisionDetectionManager* m_CollisionDetectionManagerRef)
{
	m_vec3Point = a_vec3Point;
	m_fltThreshold = a_fltThreshold;
	m_collsionObject = new CollisionObject(a_mesh);
	m_collsionObject->Initialize(m_vec3Point);
	m_CollisionDetectionManagerRef->Register(m_collsionObject);
	m_isCollisionObjectRegistered = true;
}

ControlPoint::~ControlPoint()
{}

/// <summary>
/// Get control point
/// </summary>
/// <returns></returns>
glm::vec3 ControlPoint::GetPoint()
{
	return m_vec3Point;
}

/// <summary>
/// Get derail threshold value st the point
/// </summary>
/// <returns></returns>
float ControlPoint::GetThreshold()
{
	return m_fltThreshold;
}

/// <summary>
/// Is object at control point collided?
/// </summary>
/// <returns></returns>
bool ControlPoint::IsCollided()
{
	return  m_collsionObject->IsCollided;
}

/// <summary>
/// Is control point register for any collsion detetcion? (Read-only)
/// </summary>
/// <returns></returns>
bool ControlPoint::IsCollisionObjectRegistered()
{
	return m_isCollisionObjectRegistered;
}

/// <summary>
/// Reset all properties of control point
/// </summary>
void ControlPoint::ResetControlPoint()
{
	m_isScoreAdded = false;
	m_collsionObject->ResetCollision();
}

