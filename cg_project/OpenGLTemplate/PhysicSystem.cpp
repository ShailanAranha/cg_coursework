#include "PhysicSystem.h"

/// <summary>
/// Default contructor
/// </summary>
PhysicSystem::PhysicSystem() {}

/// <summary>
/// Destructor
/// </summary>
PhysicSystem::~PhysicSystem() {}

/// <summary>
/// Initilaize object with data
/// </summary>
/// <param name="a_fltInitialSpeed"></param>
void PhysicSystem::Initialize(float a_fltInitialSpeed)
{
	m_fltInitialSpeed = a_fltInitialSpeed;
	m_fltTime = 0.0f;
}

/// <summary>
/// Game loop (Do physic calculations)
/// </summary>
/// <param name="m_deltaTime"></param>
void PhysicSystem::Update(double m_deltaTime)
{
	
	//if (m_isAccelerate || m_isBoast)
	if (m_isAccelerate)
	{
		m_fltTime +=  m_deltaTime;
		//m_fltTime += 0.0036f * m_deltaTime;
		//m_fltTime += 0.00060f* m_deltaTime;
		//m_fltTime /= 60;
	}
	else
	{
		if (m_fltTime > 0.0f)
		{
			//m_fltTime -= 0.0036f * m_deltaTime;
			m_fltTime -=  m_deltaTime;
		}
		else
		{
			m_fltTime = 0;
		}

	}

	if (m_isBoast)
	{
		m_fltTimeBoast += m_deltaTime;

	}
	else
	{
		if (m_fltTimeBoast > 0.0f)
		{
			//m_fltTime -= 0.0036f * m_deltaTime;
			m_fltTimeBoast -= m_deltaTime;
		}
		else
		{
			m_fltTimeBoast = 0;
		}
	}
}

/// <summary>
/// Start acceletaion of the object (1X)
/// </summary>
void PhysicSystem::StartAcceleration()
{
	if (Fuel <= 0)
	{
		// display low fuel
		return;
	}

	m_isAccelerate = true;
	Fuel -= FUEL_COMSUMPTION_FOR_ACCELERATION;

}

/// <summary>
/// Stop acceletaion of the object
/// </summary>
void PhysicSystem::StopAcceleration()
{
	m_isAccelerate = false;
}

/// <summary>
/// Start boost of the object (2 X Acceleration)
/// </summary>
void PhysicSystem::StartBoost()
{
	if (Fuel <= 0)
	{
		// display low fuel
		return;
	}

	m_isBoast = true;
	Fuel -= FUEL_COMSUMPTION_FOR_BOAST;
}

/// <summary>
/// Stop boost of the object
/// </summary>
void PhysicSystem::StopBoost()
{
	m_isBoast = false;
}

/// <summary>
/// Get current speed value
/// </summary>
/// <returns></returns>
float PhysicSystem::getSpeed()
{

	m_fltFinalSpeed = m_fltInitialSpeed + ACCELERATION_MAGNITUDE * m_fltTime + AccelerationDueToGravity + BOAST_MAGNITUDE * m_fltTimeBoast; // + inertia or potential/kinetic energy

	return m_fltFinalSpeed ;
}

/// <summary>
/// Calculate gravity (Accelration due to gravity)
/// </summary>
/// <param name="a_value"></param>
void PhysicSystem::CalculateGravity(float a_value)
{

	AccelerationDueToGravity = a_value * GravityMagnitue * 0.01f;
	//AccelerationDueToGravity = a_value * 2 *0.0001f;
}

/// <summary>
/// Refill fuel in the tank
/// </summary>
/// <param name="a_fltFuelAmount"></param>
void PhysicSystem::RefillFuel(float a_fltFuelAmount)
{
	float l_fltTempFuelValue = Fuel + a_fltFuelAmount;
	if (l_fltTempFuelValue >= MAX_FUEL)
	{
		Fuel = MAX_FUEL;
		return;
	}

	Fuel += a_fltFuelAmount;
}
