#include "Quad.h"

/// <summary>
/// Default constructor
/// </summary>
Quad::Quad()
{
	m_isDataUploadedToGPU = false;
}

/// <summary>
/// Destructor 
/// </summary>
Quad::~Quad()
{
	Release();
}


/// <summary>
/// Create Quad
/// </summary>
/// <param name="a_strOverlayTexture"></param>
/// <param name="a_fltScreenHeight"></param>
/// <param name="a_fltScreenWidth"></param>
void Quad::Create(string a_strOverlayTexture, float a_fltScreenHeight, float a_fltScreenWidth)
{
	m_texOverlay.Load(a_strOverlayTexture);
	m_texOverlay.SetSamplerObjectParameter(GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR);
	m_texOverlay.SetSamplerObjectParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	m_texOverlay.SetSamplerObjectParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
	m_texOverlay.SetSamplerObjectParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);

	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	// Create a VBO and Bind

	glGenBuffers(1, &m_vboId);
	glBindBuffer(GL_ARRAY_BUFFER, m_vboId);


	// VERTICES


	// Flag
	glm::vec3 l_pf0 = glm::vec3(0, 0, 0.0f);
	glm::vec3 l_pf1 = glm::vec3(0, a_fltScreenHeight,0.0f);
	glm::vec3 l_pf2 = glm::vec3(a_fltScreenWidth, 0.0f, 0.0f);
	glm::vec3 l_pf3 = glm::vec3(a_fltScreenWidth, a_fltScreenHeight, 0.0f);

	//TEXTURE POINTS



	//glm::vec2 l_fn0 = glm::vec2(0.0f, 0.0f);
	//glm::vec2 l_fn1 = glm::vec2(0.0f, 1.0f);
	//glm::vec2 l_fn2 = glm::vec2(1.0f, 0.0f);
	//glm::vec2 l_fn3 = glm::vec2(1.0f, 1.0f);

	//glm::vec2 l_fn1 = glm::vec2(1.0f, 0.0f);
	//glm::vec2 l_fn2 = glm::vec2(0.0f, 0.0f);
	//glm::vec2 l_fn3 = glm::vec2(1.0f, 1.0f);
	//glm::vec2 l_fn4 = glm::vec2(0.0f, 1.0f);

	//glm::vec2 l_fn1 = glm::vec2(1.0f, 0.0f);
	//glm::vec2 l_fn2 = glm::vec2(0.0f, 0.0f);
	//glm::vec2 l_fn3 = glm::vec2(1.0f, 1.0f);
	//glm::vec2 l_fn4 = glm::vec2(0.0f, 1.0f);

	glm::vec2 l_fn1 = glm::vec2(1.0f, 0.0f);
	glm::vec2 l_fn2 = glm::vec2(0.0f, 0.0f);
	glm::vec2 l_fn3 = glm::vec2(1.0f, 1.0f);
	glm::vec2 l_fn4 = glm::vec2(0.0f, 1.0f);



	// Vertex positions
	glm::vec3 l_arrVertices[4] =
	{

		l_pf0,l_pf2,l_pf1,l_pf3,

	};

	// Texture coordinates
	glm::vec2 l_arrTexCoords[4] =
	{
		l_fn1,l_fn2,l_fn3,l_fn4,

	};

	// Plane normal
	glm::vec3 PlusXNormal = glm::vec3(1.0f, 0.0f, 0.0f);
	glm::vec3 PlusYNormal = glm::vec3(0.0f, 1.0f, 0.0f);
	glm::vec3 PlusZNormal = glm::vec3(0.0f, 0.0f, 1.0f);
	glm::vec3 MinusXNormal = glm::vec3(-1.0f, 0.0f, 0.0f);
	glm::vec3 MinusYNormal = glm::vec3(0.0f, -1.0f, 0.0f);
	glm::vec3 MinusZNormal = glm::vec3(0.0f, 0.0f, -1.0f);

	glm::vec3 TestNormal = glm::vec3(0.0f, 1.0f, 0.0f);



	glm::vec3 l_arrNormals[4] =
	{
		// FLAG
		MinusZNormal,MinusZNormal,MinusZNormal,MinusZNormal,
	};


	// Put the vertex attributes in the VBO
	for (unsigned int i = 0; i < 4; i++) {
		AddDataToVBO(&l_arrVertices[i], sizeof(glm::vec3));
		AddDataToVBO(&l_arrTexCoords[i], sizeof(glm::vec2));
		AddDataToVBO(&l_arrNormals[i], sizeof(glm::vec3));
	}

	// Upload data to GPU
	glBufferData(GL_ARRAY_BUFFER, m_vboData.size(), &m_vboData[0], GL_STATIC_DRAW);
	m_isDataUploadedToGPU = true;
	m_vboData.clear();

	//	glFrontFace(GL_CW);
	GLsizei stride = 2 * sizeof(glm::vec3) + sizeof(glm::vec2);
	// Vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, 0);
	// Texture coordinates
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride, (void*)sizeof(glm::vec3));
	// Normal vectors
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, stride,
		(void*)(sizeof(glm::vec3) + sizeof(glm::vec2)));
}

/// <summary>
/// Render quad object
/// </summary>
void Quad::Render(bool a_isBindTexture)
{
	glBindVertexArray(m_vao);

	if (a_isBindTexture)
	{
		m_texOverlay.Bind();
	}
	// Call glDrawArrays to render each side

	// TRUSS
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	//glDrawArrays(GL_TRIANGLE_STRIP, 4, 8);

}

/// <summary>
/// Release data of quad object instance
/// </summary>
void Quad::Release()
{
	m_texOverlay.Release();
	glDeleteVertexArrays(1, &m_vao);

	glDeleteBuffers(1, &m_vboId);
	m_isDataUploadedToGPU = false;
	m_vboData.clear();
}


/// <summary>
/// Adds data to the VBO.  
/// </summary>
/// <param name="ptrData"></param>
/// <param name="dataSize"></param>
void Quad::AddDataToVBO(void* ptrData, UINT dataSize)
{
	m_vboData.insert(m_vboData.end(), (BYTE*)ptrData, (BYTE*)ptrData + dataSize);
}