#include "RacePost.h"

/// <summary>
/// Default constructor
/// </summary>
CRacePost::CRacePost()
{
	m_isDataUploadedToGPU = false;
}

/// <summary>
/// Destructor 
/// </summary>
CRacePost::~CRacePost()
{
	Release();
}

/// <summary>
/// Create racepost object
/// </summary>
/// <param name="filename"></param>
/// <param name="a_strBoardTexture"></param>
/// <param name="a_strFlagTexture"></param>
/// <param name="a_fltPostHeight"></param>
/// <param name="a_fltPostWidth"></param>
/// <param name="a_fltTextureRepeat"></param>
void CRacePost::Create(string a_strTrussTexture, string a_strBoardTexture, string a_strFlagTexture, 
	float a_fltPostHeight, float a_fltPostWidth, float a_fltTextureRepeat)
{
	m_texTruss.Load(a_strTrussTexture);
	m_texTruss.SetSamplerObjectParameter(GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR);
	m_texTruss.SetSamplerObjectParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	m_texTruss.SetSamplerObjectParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
	m_texTruss.SetSamplerObjectParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);


	m_texStartBoard.Load(a_strBoardTexture);
	m_texStartBoard.SetSamplerObjectParameter(GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR);
	m_texStartBoard.SetSamplerObjectParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	m_texStartBoard.SetSamplerObjectParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
	m_texStartBoard.SetSamplerObjectParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);


	m_textFlag.Load(a_strFlagTexture);
	m_textFlag.SetSamplerObjectParameter(GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR);
	m_textFlag.SetSamplerObjectParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	m_textFlag.SetSamplerObjectParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
	m_textFlag.SetSamplerObjectParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);
	

	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	// Create a VBO and Bind

	glGenBuffers(1, &m_vboId);
	glBindBuffer(GL_ARRAY_BUFFER, m_vboId);
	
	// VALUES

	float l_fltPostHeight = a_fltPostHeight;

	float l_fltPostWidth = a_fltPostWidth;

	float l_fltPostDistance = l_fltPostWidth/2;

	float l_fltBoardWidth = l_fltPostWidth - 4;

	float l_fltTextureRepeat = a_fltTextureRepeat;
	
	// VERTICES

	// 1.) STAND 1

	glm::vec3 l_p0 = glm::vec3(l_fltPostDistance, 0.0f, 0.0f);
	glm::vec3 l_p1 = glm::vec3(l_fltPostDistance + 2.0f, 0.0f, 0.0f);
	glm::vec3 l_p2 = glm::vec3(l_fltPostDistance, l_fltPostHeight, 0.0f);
	glm::vec3 l_p3 = glm::vec3(l_fltPostDistance + 2.0f, l_fltPostHeight, 0.0f);
	glm::vec3 l_p4 = glm::vec3(l_fltPostDistance, 0.0f, 0.0f + 2.0f);
	glm::vec3 l_p5 = glm::vec3(l_fltPostDistance + 2.0f, 0.0f, 0.0f + 2.0f);
	glm::vec3 l_p6 = glm::vec3(l_fltPostDistance, l_fltPostHeight, 0.0f + 2.0f);
	glm::vec3 l_p7 = glm::vec3(l_fltPostDistance + 2.0f, l_fltPostHeight, 0.0f + 2.0f);

	// 2.) STAND 2

	glm::vec3 l_ps0 = glm::vec3(-l_fltPostDistance, 0.0f, 0.0f);
	glm::vec3 l_ps1 = glm::vec3(-l_fltPostDistance + 2.0f, 0.0f, 0.0f);
	glm::vec3 l_ps2 = glm::vec3(-l_fltPostDistance, l_fltPostHeight, 0.0f);
	glm::vec3 l_ps3 = glm::vec3(-l_fltPostDistance + 2.0f, l_fltPostHeight, 0.0f);
	glm::vec3 l_ps4 = glm::vec3(-l_fltPostDistance, 0.0f, 0.0f + 2.0f);
	glm::vec3 l_ps5 = glm::vec3(-l_fltPostDistance + 2.0f, 0.0f, 0.0f + 2.0f);
	glm::vec3 l_ps6 = glm::vec3(-l_fltPostDistance, l_fltPostHeight, 0.0f + 2.0f);
	glm::vec3 l_ps7 = glm::vec3(-l_fltPostDistance + 2.0f, l_fltPostHeight, 0.0f + 2.0f);

	// 3.) TOP
	glm::vec3 l_pb0 = glm::vec3(-l_fltPostDistance + 2.0f, 27.0f, 0.0f);
	glm::vec3 l_pb1 = glm::vec3(l_fltPostDistance , 27.0f, 0.0f);
	glm::vec3 l_pb2 = glm::vec3(-l_fltPostDistance + 2.0f, l_fltPostHeight, 0.0f);
	glm::vec3 l_pb3 = glm::vec3(l_fltPostDistance , l_fltPostHeight, 0.0f);
	glm::vec3 l_pb4 = glm::vec3(-l_fltPostDistance + 2.0f, 27.0f, 2.0f);
	glm::vec3 l_pb5 = glm::vec3(l_fltPostDistance , 27.0f, 2.0f);
	glm::vec3 l_pb6 = glm::vec3(-l_fltPostDistance + 2.0f, l_fltPostHeight,  2.0f);
	glm::vec3 l_pb7 = glm::vec3(l_fltPostDistance , l_fltPostHeight,  2.0f);

	// START BOARD
	glm::vec3 l_pp0 = glm::vec3(-6.0f, l_fltPostHeight-8.0f, -0.3f);
	glm::vec3 l_pp1 = glm::vec3(8.0f, l_fltPostHeight - 8.0f, -0.3f);
	glm::vec3 l_pp2 = glm::vec3(-6.0f, l_fltPostHeight + 6.0f, -0.3f);
	glm::vec3 l_pp3 = glm::vec3(8.0f, l_fltPostHeight + 6.0f, -0.3f);

	// Flag
	glm::vec3 l_pf0 = glm::vec3(-8.0f, l_fltPostHeight - 10.0f, -0.2f);
	glm::vec3 l_pf1 = glm::vec3(10.0f, l_fltPostHeight - 10.0f, -0.2f);
	glm::vec3 l_pf2 = glm::vec3(-8.0f, l_fltPostHeight + 12.0f, -0.2f);
	glm::vec3 l_pf3 = glm::vec3(10.0f, l_fltPostHeight + 12.0f, -0.2f);

	//TEXTURE POINTS

	// TRUSS
	glm::vec2 l_t0 = glm::vec2(0.0f, 0.0f);
	glm::vec2 l_t1 = glm::vec2(0.0f, l_fltTextureRepeat);
	glm::vec2 l_t2 = glm::vec2(1.0f, 0.0f);
	glm::vec2 l_t3 = glm::vec2(1.0f, l_fltTextureRepeat);

	glm::vec2 l_t5 = glm::vec2(0.0f, 0.0f);
	glm::vec2 l_t6 = glm::vec2(0.0f, 1.0f);
	glm::vec2 l_t7 = glm::vec2(l_fltTextureRepeat, 0.0f);
	glm::vec2 l_t8 = glm::vec2(l_fltTextureRepeat, 1.0f);

	glm::vec2 l_t9 = glm::vec2(0.0f, 0.0f);
	glm::vec2 l_t10 = glm::vec2(0.0f, l_fltTextureRepeat);
	glm::vec2 l_t11 = glm::vec2(1.0f, 0.0f);
	glm::vec2 l_t12 = glm::vec2(1.0f, l_fltTextureRepeat);

	
	// FLAG

	glm::vec2 l_fn1 = glm::vec2(1.0f, 0.0f);
	glm::vec2 l_fn2 = glm::vec2(0.0f, 0.0f);
	glm::vec2 l_fn3 = glm::vec2(1.0f, 1.0f);
	glm::vec2 l_fn4 = glm::vec2(0.0f, 1.0f);


	// START BOARD

	glm::vec2 l_sbn1 = glm::vec2(1.0f, 0.0f);
	glm::vec2 l_sbn2 = glm::vec2(0.0f, 0.0f);
	glm::vec2 l_sbn3 = glm::vec2(1.0f, 1.0f);
	glm::vec2 l_sbn4 = glm::vec2(0.0f, 1.0f);

	

	// Vertex positions
	glm::vec3 l_arrVertices[72] =
	{
		// TRUSS
		
		// 1.) STAND 1
		l_p0,l_p2,l_p1,l_p3,

		l_p1,l_p3,l_p5,l_p7,

		l_p5,l_p7,l_p4,l_p6,

		l_p4,l_p6,l_p0,l_p2,

		l_p2,l_p6,l_p3,l_p7,

		// 2.) STAND 2
		l_ps0,l_ps2,l_ps1,l_ps3,

		l_ps1,l_ps3,l_ps5,l_ps7,

		l_ps5,l_ps7,l_ps4,l_ps6,

		l_ps4,l_ps6,l_ps0,l_ps2,

		l_ps2,l_ps6,l_ps3,l_ps7,

		// 3.) TOP
		l_pb0,l_pb2,l_pb1,l_pb3,

		l_pb1,l_pb3,l_pb5,l_pb7,

		l_pb5,l_pb7,l_pb4,l_pb6,

		l_pb4,l_pb6,l_pb0,l_pb2,

		l_pb2,l_pb6,l_pb3,l_pb7,

		l_pb0,l_pb1,l_pb4,l_pb5,

		// Flag
		//l_pf0,l_pf1,l_pf2,l_pf3,
		l_pf1,l_pf0,l_pf3,l_pf2,

		// BOARD
		l_pp1,l_pp0,l_pp3,l_pp2,
		//l_pp0,l_pp1,l_pp2,l_pp3,
	};

	// Texture coordinates
	glm::vec2 l_arrTexCoords[72] =
	{
		// TRUSS
		
		// 1.) STAND 1
		l_t0,l_t1,l_t2,l_t3,

		l_t0,l_t1,l_t2,l_t3,

		l_t0,l_t1,l_t2,l_t3,

		l_t0,l_t1,l_t2,l_t3,

		l_t0,l_t1,l_t2,l_t3,


		// 2.) STAND 2
		l_t0,l_t1,l_t2,l_t3,

		l_t0,l_t1,l_t2,l_t3,

		l_t0,l_t1,l_t2,l_t3,

		l_t0,l_t1,l_t2,l_t3,

		l_t0,l_t1,l_t2,l_t3,

	
		// 3.) TOP
		l_t5,l_t6,l_t7,l_t8,

		l_t5,l_t6,l_t7,l_t8,

		l_t5,l_t6,l_t7,l_t8,

		l_t5,l_t6,l_t7,l_t8,

		l_t5,l_t6,l_t7,l_t8,

		l_t9,l_t10,l_t11,l_t12,


		// FLAG
		l_fn1,l_fn2,l_fn3,l_fn4,

		// START BOARD
		//l_sbn1,l_sbn2,l_sbn3,l_sbn4,
		l_sbn2,l_sbn1,l_sbn4,l_sbn3,
	};

	// Plane normal
	glm::vec3 PlusXNormal = glm::vec3(1.0f, 0.0f, 0.0f);
	glm::vec3 PlusYNormal = glm::vec3(0.0f, 1.0f, 0.0f);
	glm::vec3 PlusZNormal = glm::vec3(0.0f, 0.0f, 1.0f);
	glm::vec3 MinusXNormal = glm::vec3(-1.0f, 0.0f, 0.0f);
	glm::vec3 MinusYNormal = glm::vec3(0.0f, -1.0f, 0.0f);
	glm::vec3 MinusZNormal = glm::vec3(0.0f, 0.0f, -1.0f);

	glm::vec3 TestNormal = glm::vec3(0.0f, 1.0f, 0.0f);



	glm::vec3 l_arrNormals[72] =
	{
		// TRUSS

		// 1.) STAND 1
		MinusZNormal,MinusZNormal,MinusZNormal,MinusZNormal,
		PlusXNormal,PlusXNormal,PlusXNormal,PlusXNormal,
		PlusZNormal,PlusZNormal,PlusZNormal,PlusZNormal,
		MinusXNormal,MinusXNormal,MinusXNormal,MinusXNormal,
		PlusYNormal,PlusYNormal,PlusYNormal,PlusYNormal,

		// 2.) STAND 2
		MinusZNormal,MinusZNormal,MinusZNormal,MinusZNormal,
		PlusXNormal,PlusXNormal,PlusXNormal,PlusXNormal,
		PlusZNormal,PlusZNormal,PlusZNormal,PlusZNormal,
		MinusXNormal,MinusXNormal,MinusXNormal,MinusXNormal,
		PlusYNormal,PlusYNormal,PlusYNormal,PlusYNormal,

		// 3.) TOP
		MinusZNormal,MinusZNormal,MinusZNormal,MinusZNormal,
		PlusXNormal,PlusXNormal,PlusXNormal,PlusXNormal,
		PlusZNormal,PlusZNormal,PlusZNormal,PlusZNormal,
		MinusXNormal,MinusXNormal,MinusXNormal,MinusXNormal,
		PlusYNormal,PlusYNormal,PlusYNormal,PlusYNormal,

		MinusYNormal,MinusYNormal,MinusYNormal,MinusYNormal,

		// FLAG
		MinusZNormal,MinusZNormal,MinusZNormal,MinusZNormal,

		// START BOARD
		MinusZNormal,MinusZNormal,MinusZNormal,MinusZNormal,
	};


	// Put the vertex attributes in the VBO
	for (unsigned int i = 0; i < 72; i++) {
		AddDataToVBO(&l_arrVertices[i], sizeof(glm::vec3));
		AddDataToVBO(&l_arrTexCoords[i], sizeof(glm::vec2)); 
		AddDataToVBO(&l_arrNormals[i], sizeof(glm::vec3));
	}

	// Upload data to GPU
	glBufferData(GL_ARRAY_BUFFER, m_vboData.size(), &m_vboData[0], GL_STATIC_DRAW);
	m_isDataUploadedToGPU = true;
	m_vboData.clear();

	//	glFrontFace(GL_CW);
	GLsizei stride = 2 * sizeof(glm::vec3) + sizeof(glm::vec2);
	// Vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, 0);
	// Texture coordinates
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride, (void*)sizeof(glm::vec3));
	// Normal vectors
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, stride,
		(void*)(sizeof(glm::vec3) + sizeof(glm::vec2)));
}

/// <summary>
/// Render racepost object
/// </summary>
void CRacePost::Render()
{
	glBindVertexArray(m_vao);
	m_texTruss.Bind();
	// Call glDrawArrays to render each side

	// TRUSS

	// 1.) STAND 1
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 4, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 8, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 12, 4);

	// 2.) STAND 2
	glDrawArrays(GL_TRIANGLE_STRIP, 16, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 20, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 24, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 28, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 32, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 36, 4);

	// 3.) TOP
	glDrawArrays(GL_TRIANGLE_STRIP, 40, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 44, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 48, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 52, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 56, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 60, 4);

	m_textFlag.Bind();
	glDrawArrays(GL_TRIANGLE_STRIP, 64, 4);

	m_texStartBoard.Bind();
	glDrawArrays(GL_TRIANGLE_STRIP, 68, 4);

}

/// <summary>
/// Release data of racepost object instance
/// </summary>
void CRacePost::Release()
{
	m_texTruss.Release();
	m_textFlag.Release();
	m_texStartBoard.Release();
	glDeleteVertexArrays(1, &m_vao);

	glDeleteBuffers(1, &m_vboId);
	m_isDataUploadedToGPU = false;
	m_vboData.clear();
}


/// <summary>
/// Adds data to the VBO.  
/// </summary>
/// <param name="ptrData"></param>
/// <param name="dataSize"></param>
void CRacePost::AddDataToVBO(void* ptrData, UINT dataSize)
{
	m_vboData.insert(m_vboData.end(), (BYTE*)ptrData, (BYTE*)ptrData + dataSize);
}