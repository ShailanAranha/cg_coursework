
#include "CollisionDetectionManager.h"

/// <summary>
/// Default constructor
/// </summary>
CollisionDetectionManager::CollisionDetectionManager() 
{
	//m_fltThreshold = 1.5f;
}

/// <summary>
/// Destructor
/// </summary>
CollisionDetectionManager::~CollisionDetectionManager() {}

/// <summary>
/// Register colliable object to the list to detect collision
/// </summary>
/// <param name="a_CollisionObject"></param>
void CollisionDetectionManager::Register(CollisionObject* a_CollisionObject)
{
	m_lstCollidables.push_back(a_CollisionObject);
}

/// <summary>
/// Game loop, check the distance between each collidable object regitered and the player position and triggers collision if the value is less than threshold
/// </summary>
/// <param name="a_playerPosition"></param>
void CollisionDetectionManager::Update(glm::vec3 a_playerPosition)
{
	for (size_t i = 0; i < m_lstCollidables.size(); i++)
	{
		m_fltTest = glm::length(a_playerPosition - m_lstCollidables[i]->GetPosition());

		if ( glm::length(a_playerPosition - m_lstCollidables[i]->GetPosition()) < m_fltThreshold)
		{
			m_lstCollidables[i]->IsCollided = true;
		}
	}
}

/// <summary>
/// Change the threshold for collision
/// </summary>
/// <param name="a_fltThreshold"></param>
void CollisionDetectionManager::ChangeThreshold(float a_fltThreshold)
{
	m_fltThreshold = a_fltThreshold;
}
