#include "ControlPointManager.h"

/// <summary>
/// Default constructor
/// </summary>
ControlPointManager::ControlPointManager()
{
	m_CCatmullRomRef = NULL;
	m_CCatmullRomWithOffsetRef = NULL;
}

/// <summary>
/// Destructor
/// </summary>
ControlPointManager::~ControlPointManager()
{

}

/// <summary>
/// Initlaize with data
/// </summary>
/// <param name="a_pCoinMesh"></param>
/// <param name="a_CollisionDetectionManagerRef"></param>
void ControlPointManager::Initialize(COpenAssetImportMesh* a_pCoinMesh, CollisionDetectionManager* a_CollisionDetectionManagerRef)
{
	// TRACK POINTS
	m_controlPoints.push_back(ControlPoint(m_vec3Point1, 10.0f));
	m_controlPoints.push_back(ControlPoint(m_vec3Point2, 10.0f));
	m_controlPoints.push_back(ControlPoint(m_vec3Point3, 10.0f));
	m_controlPoints.push_back(ControlPoint(m_vec3Point4, 10.0f));
	m_controlPoints.push_back(ControlPoint(m_vec3Point5, 0.20f, a_pCoinMesh, a_CollisionDetectionManagerRef));

	m_controlPoints.push_back(ControlPoint(m_vec3Point6, 0.20f, a_pCoinMesh, a_CollisionDetectionManagerRef));
	m_controlPoints.push_back(ControlPoint(m_vec3Point7, 0.25f, a_pCoinMesh, a_CollisionDetectionManagerRef));
	m_controlPoints.push_back(ControlPoint(m_vec3Point8, 0.26f));
	m_controlPoints.push_back(ControlPoint(m_vec3Point9, 0.16f, a_pCoinMesh, a_CollisionDetectionManagerRef));
	m_controlPoints.push_back(ControlPoint(m_vec3Point10, 0.21f, a_pCoinMesh, a_CollisionDetectionManagerRef));
	m_controlPoints.push_back(ControlPoint(m_vec3Point11, 0.27f));
	m_controlPoints.push_back(ControlPoint(m_vec3Point12, 0.20f, a_pCoinMesh, a_CollisionDetectionManagerRef));
	m_controlPoints.push_back(ControlPoint(m_vec3Point13, 0.27f, a_pCoinMesh, a_CollisionDetectionManagerRef));
	m_controlPoints.push_back(ControlPoint(m_vec3Point14, 0.30f, a_pCoinMesh, a_CollisionDetectionManagerRef));

	//spin
	m_controlPoints.push_back(ControlPoint(m_vec3Point15, 0.22f));
	m_controlPoints.push_back(ControlPoint(m_vec3Point16, 0.20f, a_pCoinMesh, a_CollisionDetectionManagerRef));
	m_controlPoints.push_back(ControlPoint(m_vec3Point17, 0.21f, a_pCoinMesh, a_CollisionDetectionManagerRef));
	m_controlPoints.push_back(ControlPoint(m_vec3Point18, 0.25f));
	m_controlPoints.push_back(ControlPoint(m_vec3Point19, 0.27f, a_pCoinMesh, a_CollisionDetectionManagerRef));

	m_controlPoints.push_back(ControlPoint(m_vec3Point20, 0.20f, a_pCoinMesh, a_CollisionDetectionManagerRef));
	m_controlPoints.push_back(ControlPoint(m_vec3Point21, 0.25f, a_pCoinMesh, a_CollisionDetectionManagerRef));
	m_controlPoints.push_back(ControlPoint(m_vec3Point22, 0.20f, a_pCoinMesh, a_CollisionDetectionManagerRef));
	m_controlPoints.push_back(ControlPoint(m_vec3Point23, 0.22f));
	m_controlPoints.push_back(ControlPoint(m_vec3Point24, 0.20f, a_pCoinMesh, a_CollisionDetectionManagerRef));
	m_controlPoints.push_back(ControlPoint(m_vec3Point25, 0.20f, a_pCoinMesh, a_CollisionDetectionManagerRef));
	m_controlPoints.push_back(ControlPoint(m_vec3Point26, 0.17f, a_pCoinMesh, a_CollisionDetectionManagerRef));

	m_controlPoints.push_back(ControlPoint(m_vec3Point27, 0.20f));
	m_controlPoints.push_back(ControlPoint(m_vec3Point28, 0.20f, a_pCoinMesh, a_CollisionDetectionManagerRef));


	vector<glm::vec3> l_controlPoints;

	for (int i = 0; i < m_controlPoints.size(); i++)
	{
		l_controlPoints.push_back(m_controlPoints[i].GetPoint());
		//m_controlPointsWithOffset.push_back(m_controlPoints[i].GetPoint() + m_offsetValue);
	}
	m_controlPointsWithOffset =
	{
		/*
		*/
		m_vec3Point1 + m_offsetValue, m_vec3Point2 + m_offsetValue,
		m_vec3Point3 + m_offsetValue, m_vec3Point4 + m_offsetValue,
		m_vec3Point5 + m_offsetValue,m_vec3Point6 + m_offsetValue,
		m_vec3Point7 + m_offsetValue, m_vec3Point8 + m_offsetValue ,
		m_vec3Point9 + m_offsetValue ,m_vec3Point10 + m_offsetValue,
		m_vec3Point11 + m_offsetValue , m_vec3Point12 + m_offsetValue ,
		m_vec3Point13 + m_offsetValue , m_vec3Point14 + m_offsetValue,
		m_vec3Point15 + m_offsetValue, m_vec3Point16 + m_offsetValue,
		m_vec3Point17 + m_offsetValue,m_vec3Point18 + m_offsetValue,
		m_vec3Point19 + m_offsetValue,m_vec3Point20 + m_offsetValue,
		m_vec3Point21 + m_offsetValue, m_vec3Point22 + m_offsetValue,
		m_vec3Point23 + m_offsetValue, m_vec3Point24 + m_offsetValue,
		m_vec3Point25 + m_offsetValue, m_vec3Point26 + m_offsetValue,
		m_vec3Point27 + m_offsetValue, m_vec3Point28 + m_offsetValue,
	};

	
	// MAIN TRACK
	m_CCatmullRomRef = new CCatmullRom();

	m_CCatmullRomRef->CreateCentreline(l_controlPoints);
	m_CCatmullRomRef->CreateOffsetCurves(glm::vec3(0, 1, 0));

	/*
	Track: https://www.reddit.com/r/Fusion360/comments/jd46ao/how_would_you_create_a_heartlined_roller_coaster/[25/03/2022]
	*/
	m_CCatmullRomRef->CreateTrack("resources\\textures\\rollar_coaster_track.png");

	// OFFSET TRACK
	m_CCatmullRomWithOffsetRef = new CCatmullRom();
	m_CCatmullRomWithOffsetRef->CreateCentreline(m_controlPointsWithOffset);


	m_CCatmullRomWithOffsetRef->CreateOffsetCurves(glm::vec3(0, 1, 0));

	/*
	Track: https://www.reddit.com/r/Fusion360/comments/jd46ao/how_would_you_create_a_heartlined_roller_coaster/[25/03/2022]
	*/
	m_CCatmullRomWithOffsetRef->CreateTrack("resources\\textures\\rollar_coaster_track.png");

}

