#pragma once

#include "Common.h"
#include "Texture.h"
#include "VertexBufferObject.h"

// Class for generating a xz plane of a given size
class Quad
{
public:

	/// <summary>
	/// Default constructor
	/// </summary>
	Quad();

	/// <summary>
	/// Destructor 
	/// </summary>
	~Quad();

	/// <summary>
	/// Create Quad
	/// </summary>
	/// <param name="a_strOverlayTexture"></param>
	/// <param name="a_fltScreenHeight"></param>
	/// <param name="a_fltScreenWidth"></param>
	void Create(string a_strOverlayTexture, float a_fltScreenHeight, float a_fltScreenWidth);

	/// <summary>
	/// Render quad object
	/// </summary>
	void Render(bool a_isBindTexture);

	/// <summary>
	/// Release data of quad object instance
	/// </summary>
	void Release();

private:

	/// <summary>
	/// VAO reference object
	/// </summary>
	GLuint m_vao;

	/// <summary>
	/// VBO Id
	/// </summary>
	UINT m_vboId;

	/// <summary>
	/// VBO Data
	/// </summary>
	vector<BYTE> m_vboData;

	/// <summary>
	/// Is data uploaded to GPU?
	/// </summary>
	bool m_isDataUploadedToGPU;

	/// <summary>
	/// Add data to VBO data object (Helper function)
	/// </summary>
	/// <param name="ptrData"></param>
	/// <param name="dataSize"></param>
	void AddDataToVBO(void* ptrData, UINT dataSize);

	/// <summary>
	/// Texture references for the object
	/// </summary>
	CTexture m_texOverlay;
};
