#include "RacePostHolder.h"

/// <summary>
/// Default constructor
/// </summary>
CRacePostHolder::CRacePostHolder()
{
	m_isDataUploadedToGPU = false;
}

/// <summary>
/// Destructor 
/// </summary>
CRacePostHolder::~CRacePostHolder()
{
	Release();
}

/// <summary>
/// Create racepost object
/// </summary>
/// <param name="filename"></param>
void CRacePostHolder::Create(string a_strTexture)
{
	m_texMetal.Load(a_strTexture);
	m_texMetal.SetSamplerObjectParameter(GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR);
	m_texMetal.SetSamplerObjectParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	m_texMetal.SetSamplerObjectParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
	m_texMetal.SetSamplerObjectParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);


	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	// Create a VBO and Bind

	glGenBuffers(1, &m_vboId);
	glBindBuffer(GL_ARRAY_BUFFER, m_vboId);

	// VERTICES

	// 1.) BASE 1

	glm::vec3 l_p0 = glm::vec3(0, 10.0f, 0.0f);
	glm::vec3 l_p1 = glm::vec3(8.0f, 10.0f, 0.0f);
	glm::vec3 l_p2 = glm::vec3(0, 13.0f, 0.0f);
	glm::vec3 l_p3 = glm::vec3(8.0f, 13.0f, 0.0f);
	glm::vec3 l_p4 = glm::vec3(0, 10.0f, 8.0f);
	glm::vec3 l_p5 = glm::vec3(8.0f, 10.0f, 8.0f);
	glm::vec3 l_p6 = glm::vec3(0, 13.0f, 8.0f);
	glm::vec3 l_p7 = glm::vec3(8.0f, 13.0f, 8.0f);

	// 2.) BASE 2

	glm::vec3 l_ps0 = glm::vec3(40, 10.0f, 0.0f);
	glm::vec3 l_ps1 = glm::vec3(48.0f, 10.0f, 0.0f);
	glm::vec3 l_ps2 = glm::vec3(40, 13.0f, 0.0f);
	glm::vec3 l_ps3 = glm::vec3(48.0f, 13.0f, 0.0f);
	glm::vec3 l_ps4 = glm::vec3(40, 10.0f, 8.0f);
	glm::vec3 l_ps5 = glm::vec3(48.0f, 10.0f, 8.0f);
	glm::vec3 l_ps6 = glm::vec3(40, 13.0f, 8.0f);
	glm::vec3 l_ps7 = glm::vec3(48.0f, 13.0f, 8.0f);

	//  SUPPORT 1
	glm::vec3 l_s1 = glm::vec3(2, 10.0f, 2.0f);
	glm::vec3 l_s2 = glm::vec3(6.0f, 10.0f, 2.0f);
	glm::vec3 l_s3 = glm::vec3(2, 10.0f, 6.0f);
	glm::vec3 l_s4 = glm::vec3(6.0f, 10.0f, 6.0f);

	//  SUPPORT 2

	glm::vec3 l_r1 = glm::vec3(42, 10.0f, 2.0f);
	glm::vec3 l_r2 = glm::vec3(46.0f, 10.0f, 2.0f);
	glm::vec3 l_r3 = glm::vec3(42, 10.0f, 6.0f);
	glm::vec3 l_r4 = glm::vec3(46.0f, 10.0f, 6.0f);
	

	//  COMMON POINT
	glm::vec3 l_c1 = glm::vec3(24.0f, 4.0f, 6.0f);
	glm::vec3 l_c2 = glm::vec3(24.0f, 4.0f, 2.0f);
	glm::vec3 l_c3 = glm::vec3(24.0f, 0.0f, 6.0f);
	glm::vec3 l_c4 = glm::vec3(24.0f, 0.0f, 2.0f);

	//TEXTURE POINTS

	glm::vec2 l_t0 = glm::vec2(0.0f, 0.0f);
	glm::vec2 l_t1 = glm::vec2(0.0f, 1.0f);
	glm::vec2 l_t2 = glm::vec2(1.0f, 0.0f);
	glm::vec2 l_t3 = glm::vec2(1.0f, 1.0f);

	glm::vec2 l_t5 = glm::vec2(0.0f, 0.0f);
	glm::vec2 l_t6 = glm::vec2(0.0f, 1.0f);
	glm::vec2 l_t7 = glm::vec2(1.0f, 0.0f);
	glm::vec2 l_t8 = glm::vec2(1.0f, 1.0f);

	glm::vec2 l_t9 = glm::vec2(0.0f, 0.0f);
	glm::vec2 l_t10 = glm::vec2(0.0f, 1.0f);
	glm::vec2 l_t11 = glm::vec2(1.0f, 0.0f);
	glm::vec2 l_t12 = glm::vec2(1.0f, 1.0f);




	// Vertex positions
	glm::vec3 l_arrVertices[80] =
	{
		// TRUSS

		// 1.) BASE 1
		l_p0,l_p2,l_p1,l_p3, // front

		l_p1,l_p3,l_p5,l_p7, // right

		l_p5,l_p7,l_p4,l_p6, // back

		l_p4,l_p6,l_p0,l_p2, // left

		l_p2,l_p6,l_p3,l_p7, // top

		// 2.) BASE 2
		l_ps0,l_ps2,l_ps1,l_ps3,

		l_ps1,l_ps3,l_ps5,l_ps7,

		l_ps5,l_ps7,l_ps4,l_ps6,

		l_ps4,l_ps6,l_ps0,l_ps2,

		l_ps2,l_ps6,l_ps3,l_ps7,

		//  SUPPORT 1
		l_c1,l_s4,l_c3,l_s3, // front 
		l_c2,l_s2,l_c1,l_s4, //right
		l_c4,l_s1,l_c2,l_s2, //back
		l_c3,l_s3,l_c4,l_s1, // left 
		l_s2,l_s1,l_s4,l_s3, //top
 
		//  SUPPORT 2
		l_c3,l_r4,l_c1,l_r3, // front 
		l_c1,l_r3,l_c2,l_r1, //right
		l_c2,l_r1,l_c4,l_r2, //back
		l_c4,l_r2,l_c3,l_r4, // left 
		l_r2,l_r1,l_r4,l_r3, //top
	};

	// Texture coordinates
	glm::vec2 l_arrTexCoords[80] =
	{
		// TRUSS

		// 1.) BASE 1
		l_t0,l_t1,l_t2,l_t3,

		l_t0,l_t1,l_t2,l_t3,

		l_t0,l_t1,l_t2,l_t3,

		l_t0,l_t1,l_t2,l_t3,

		l_t0,l_t1,l_t2,l_t3,


		// 2.) BASE 2
		l_t0,l_t1,l_t2,l_t3,

		l_t0,l_t1,l_t2,l_t3,

		l_t0,l_t1,l_t2,l_t3,

		l_t0,l_t1,l_t2,l_t3,

		l_t0,l_t1,l_t2,l_t3,

		//  SUPPORT 1

		l_t0,l_t1,l_t2,l_t3,
		l_t0,l_t1,l_t2,l_t3,
		l_t0,l_t1,l_t2,l_t3,
		l_t0,l_t1,l_t2,l_t3,
		l_t0,l_t1,l_t2,l_t3,

		//  SUPPORT 2
		l_t0,l_t1,l_t2,l_t3,
		l_t0,l_t1,l_t2,l_t3,
		l_t0,l_t1,l_t2,l_t3,
		l_t0,l_t1,l_t2,l_t3,
		l_t0,l_t1,l_t2,l_t3,

	
	};

	// Plane normal
	glm::vec3 PlusXNormal = glm::vec3(1.0f, 0.0f, 0.0f);
	glm::vec3 PlusYNormal = glm::vec3(0.0f, 1.0f, 0.0f);
	glm::vec3 PlusZNormal = glm::vec3(0.0f, 0.0f, 1.0f);
	glm::vec3 MinusXNormal = glm::vec3(-1.0f, 0.0f, 0.0f);
	glm::vec3 MinusYNormal = glm::vec3(0.0f, -1.0f, 0.0f);
	glm::vec3 MinusZNormal = glm::vec3(0.0f, 0.0f, -1.0f);

	glm::vec3 TestNormal = glm::vec3(0.0f, 1.0f, 0.0f);



	glm::vec3 l_arrNormals[80] =
	{
		// TRUSS

		// 1.) BASE 1
		MinusZNormal,MinusZNormal,MinusZNormal,MinusZNormal,
		PlusXNormal,PlusXNormal,PlusXNormal,PlusXNormal,
		PlusZNormal,PlusZNormal,PlusZNormal,PlusZNormal,
		MinusXNormal,MinusXNormal,MinusXNormal,MinusXNormal,
		PlusYNormal,PlusYNormal,PlusYNormal,PlusYNormal,

		// 2.) BASE 2
		MinusZNormal,MinusZNormal,MinusZNormal,MinusZNormal,
		PlusXNormal,PlusXNormal,PlusXNormal,PlusXNormal,
		PlusZNormal,PlusZNormal,PlusZNormal,PlusZNormal,
		MinusXNormal,MinusXNormal,MinusXNormal,MinusXNormal,
		PlusYNormal,PlusYNormal,PlusYNormal,PlusYNormal,

		//  SUPPORT 1
		PlusXNormal,PlusXNormal,PlusXNormal,PlusXNormal,
		MinusZNormal,MinusZNormal,MinusZNormal,MinusZNormal,
		MinusXNormal,MinusXNormal,MinusXNormal,MinusXNormal,
		PlusZNormal,PlusZNormal,PlusZNormal,PlusZNormal,
		PlusYNormal,PlusYNormal,PlusYNormal,PlusYNormal,

		//  SUPPORT 2
		PlusXNormal,PlusXNormal,PlusXNormal,PlusXNormal,
		MinusZNormal,MinusZNormal,MinusZNormal,MinusZNormal,
		MinusXNormal,MinusXNormal,MinusXNormal,MinusXNormal,
		PlusZNormal,PlusZNormal,PlusZNormal,PlusZNormal,
		PlusYNormal,PlusYNormal,PlusYNormal,PlusYNormal,

	};


	// Put the vertex attributes in the VBO
	for (unsigned int i = 0; i < 80; i++) {
		AddDataToVBO(&l_arrVertices[i], sizeof(glm::vec3));
		AddDataToVBO(&l_arrTexCoords[i], sizeof(glm::vec2));
		AddDataToVBO(&l_arrNormals[i], sizeof(glm::vec3));
	}

	// Upload data to GPU
	glBufferData(GL_ARRAY_BUFFER, m_vboData.size(), &m_vboData[0], GL_STATIC_DRAW);
	m_isDataUploadedToGPU = true;
	m_vboData.clear();

	//	glFrontFace(GL_CW);
	GLsizei stride = 2 * sizeof(glm::vec3) + sizeof(glm::vec2);
	// Vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, 0);
	// Texture coordinates
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride, (void*)sizeof(glm::vec3));
	// Normal vectors
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, stride,
		(void*)(sizeof(glm::vec3) + sizeof(glm::vec2)));
}

/// <summary>
/// Render racepost object
/// </summary>
void CRacePostHolder::Render()
{
	glBindVertexArray(m_vao);
	m_texMetal.Bind();
	// Call glDrawArrays to render each side

	// 1.) BASE 1
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 4, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 8, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 12, 4);

	// 2.) BASE 2
	glDrawArrays(GL_TRIANGLE_STRIP, 16, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 20, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 24, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 28, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 32, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 36, 4);

	
	//  SUPPORT 1

	glDrawArrays(GL_TRIANGLE_STRIP, 40, 4);
	glDrawArrays(GL_TRIANGLE_STRIP, 44, 4);
	glDrawArrays(GL_TRIANGLE_STRIP, 48, 4);
	glDrawArrays(GL_TRIANGLE_STRIP, 52, 4);
	glDrawArrays(GL_TRIANGLE_STRIP, 56, 4);

	//  SUPPORT 2

	glDrawArrays(GL_TRIANGLE_STRIP, 60, 4);
	glDrawArrays(GL_TRIANGLE_STRIP, 64, 4);
	glDrawArrays(GL_TRIANGLE_STRIP, 68, 4);
	glDrawArrays(GL_TRIANGLE_STRIP, 72, 4);
	glDrawArrays(GL_TRIANGLE_STRIP, 76, 4);

}

/// <summary>
/// Release data of racepost object instance
/// </summary>
void CRacePostHolder::Release()
{
	m_texMetal.Release();
	glDeleteVertexArrays(1, &m_vao);

	glDeleteBuffers(1, &m_vboId);
	m_isDataUploadedToGPU = false;
	m_vboData.clear();
}


/// <summary>
/// Adds data to the VBO.  
/// </summary>
/// <param name="ptrData"></param>
/// <param name="dataSize"></param>
void CRacePostHolder::AddDataToVBO(void* ptrData, UINT dataSize)
{
	m_vboData.insert(m_vboData.end(), (BYTE*)ptrData, (BYTE*)ptrData + dataSize);
}