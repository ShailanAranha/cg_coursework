#include "CollisionObject.h"

/// <summary>
/// Parameterized contructor
/// </summary>
/// <param name="a_object"></param>
CollisionObject::CollisionObject(COpenAssetImportMesh* a_object)
{
	m_object = a_object;
}

/// <summary>
/// Parameterized contructor
/// </summary>
/// <param name="a_object"></param>
CollisionObject::CollisionObject(COpenAssetImportMesh* a_object, glm::vec3 a_position) : m_object(a_object), m_position(a_position)
{
	//m_object = a_object;
	//m_position = a_position;
}

/// <summary>
/// Destructor
/// </summary>
CollisionObject::~CollisionObject() {}


/// <summary>
/// Initaltate object with data
/// </summary>
/// <param name="a_position"></param>
void CollisionObject::Initialize(glm::vec3 a_position)
{
	m_position = a_position;
}

/// <summary>
/// Get position of the object (Read-Only)
/// </summary>
/// <returns></returns>
glm::vec3 CollisionObject::GetPosition()
{
	return m_position;
}

/// <summary>
/// Get instance of the object (Read-Only)
/// </summary>
/// <returns></returns>
COpenAssetImportMesh* CollisionObject::GetObject()
{
	return m_object;
}

/// <summary>
/// Reset collision info (Reset value to non-collsion state)
/// </summary
void CollisionObject::ResetCollision()
{
	IsCollided = false;
}