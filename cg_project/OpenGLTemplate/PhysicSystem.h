#pragma once
#include "Common.h"
#include "OpenAssetImportMesh.h"

// THIS PHYSIC SYSTEM WILL ONLY HAVE SCALAR QUNTITIES (MAGNITUDE) AS THE DIRECTION IS ALREADY SET BASED ON THE PATH

class PhysicSystem
{
public:

	/// <summary>
	/// Default contructor
	/// </summary>
	PhysicSystem();

	/// <summary>
	/// Destructor
	/// </summary>
	~PhysicSystem();

	//void Initialize(glm::vec3 a_position, float a_fltMass);
	//void Update(double m_deltaTime);

	//glm::vec3 Position = glm::vec3(0,0,0);
	//glm::vec3 Velocity = glm::vec3(0, 0, 0);
	//glm::vec3 Force = glm::vec3(0, 0, 0);
	//float Mass = 0.1f;

	/// <summary>
	/// Initilaize object with data
	/// </summary>
	/// <param name="a_fltInitialSpeed"></param>
	void Initialize(float a_fltInitialSpeed);

	/// <summary>
	/// Game loop (Do physic calculations)
	/// </summary>
	/// <param name="m_deltaTime"></param>
	void Update(double m_deltaTime);

	/// <summary>
	/// Start acceletaion of the object (1X)
	/// </summary>
	void StartAcceleration();
	
	/// <summary>
	/// Stop acceletaion of the object
	/// </summary>
	void StopAcceleration();

	/// <summary>
	/// Start boost of the object (2 X Acceleration)
	/// </summary>
	void StartBoost();

	/// <summary>
	/// Stop boost of the object
	/// </summary>
	void StopBoost();

	/// <summary>
	/// Get current speed value
	/// </summary>
	/// <returns></returns>
	float getSpeed();

	/// <summary>
	/// Calculate gravity (Accelration due to gravity)
	/// </summary>
	/// <param name="a_value"></param>
	void CalculateGravity(float a_value);

	/// <summary>
	/// Refill fuel in the tank
	/// </summary>
	/// <param name="a_fltFuelAmount"></param>
	void RefillFuel(float a_fltFuelAmount);

	/// <summary>
	///   less fuel for acceleration and more fuel for boast
	/// </summary>
	float Fuel = 100.0f;

private:

	/// <summary>
	/// Acceleration due to gravity
	/// </summary>
	glm::vec3 Gravity = glm::vec3(0, -9.8f, 0);

	/// <summary>
	/// Gravity magnitue 
	/// </summary>
	float GravityMagnitue = -9.8f;

	/// <summary>
	/// Current acceleration due to gravity value
	/// </summary>
	float AccelerationDueToGravity = 0.0f;

	/// <summary>
	/// Initiate speed of the object
	/// </summary>
	float m_fltInitialSpeed = 0.0f;

	/// <summary>
	/// Final speed of the object
	/// </summary>
	float m_fltFinalSpeed = 0.0f;

	/// <summary>
	/// Boost time
	/// </summary>
	float m_fltTimeBoast = 0.0f;

	/// <summary>
	/// Current acceleration value
	/// </summary>
	float m_fltAcceleration = 0.0f;

	/// <summary>
	/// Acceleration time
	/// </summary>
	float m_fltTime = 0.0f;

	/// <summary>
	/// Fiction magnitue
	/// </summary>
	float fiction = 0.01f;

	/// <summary>
	/// Fuel consumption for acceleration
	/// </summary>
	const float FUEL_COMSUMPTION_FOR_ACCELERATION = 0.1f;
	
	/// <summary>
	/// Fuel consumption for boost
	/// </summary>
	const float FUEL_COMSUMPTION_FOR_BOAST = 0.2f;

	/// <summary>
	/// Max fuel tank capacity
	/// </summary>
	const float MAX_FUEL = 100.0f;

	/// <summary>
	/// Acceleration magnitue
	/// </summary>
	const float ACCELERATION_MAGNITUDE = 0.00001f;

	/// <summary>
	/// Is acceleration active?
	/// </summary>
	bool m_isAccelerate = false;

	/// <summary>
	/// Boost magnitue
	/// </summary>
	//const float BOAST_MAGNITUDE = 0.005f;
	const float BOAST_MAGNITUDE = 0.00005f;

	/// <summary>
	/// Is boost active?
	/// </summary>
	bool m_isBoast = false;

};
