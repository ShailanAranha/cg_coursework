#include "CatmullRom.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>


/// <summary>
/// Default contructor
/// </summary>
CCatmullRom::CCatmullRom()
{
	m_vertexCount = 0;
}

CCatmullRom::~CCatmullRom()
{}

/// <summary>
///  Perform Catmull Rom spline interpolation between four points, interpolating the space between p1 and p2
/// </summary>
/// <param name="p0"></param>
/// <param name="p1"></param>
/// <param name="p2"></param>
/// <param name="p3"></param>
/// <param name="t"></param>
/// <returns></returns>
glm::vec3 CCatmullRom::Interpolate(glm::vec3 &p0, glm::vec3 &p1, glm::vec3 &p2, glm::vec3 &p3, float t)
{
    float t2 = t * t;
    float t3 = t2 * t;

	glm::vec3 a = p1;
	glm::vec3 b = 0.5f * (-p0 + p2);
	glm::vec3 c = 0.5f * (2.0f*p0 - 5.0f*p1 + 4.0f*p2 - p3);
	glm::vec3 d = 0.5f * (-p0 + 3.0f*p1 - 3.0f*p2 + p3);

	return a + b*t + c*t2 + d*t3;

}

/// <summary>
/// Set control points
/// </summary>
/// <param name="a_controlPoints"></param>
void CCatmullRom::SetControlPoints(const vector<glm::vec3>& a_controlPoints)
{
	m_controlPoints = a_controlPoints;

	//for (int i = 0; i < m_controlPoints.size(); i++)
	//{
	//	m_controlPoints_with_offset.push_back(m_controlPoints[i] + m_offsetValue);
	//}


	// Optionally, set upvectors (m_controlUpVectors, one for each control point as well)
}

/// <summary>
///Determine lengths along the control points, which is the set of control points forming the closed curve 
/// </summary>
void CCatmullRom::ComputeLengthsAlongControlPoints()
{
	int M = (int) m_controlPoints.size();

	float fAccumulatedLength = 0.0f;
	m_distances.push_back(fAccumulatedLength);
	for (int i = 1; i < M; i++) {
		fAccumulatedLength += glm::distance(m_controlPoints[i-1], m_controlPoints[i]);
		m_distances.push_back(fAccumulatedLength);
	}

	// Get the distance from the last point to the first
	fAccumulatedLength += glm::distance(m_controlPoints[M-1], m_controlPoints[0]);
	m_distances.push_back(fAccumulatedLength);
}


/// <summary>
///  Return a point on the centreline based on a certain distance along the control curve.
/// </summary>
/// <param name="d"></param>
/// <param name="p"></param>
/// <param name="up"></param>
/// <returns></returns>
bool CCatmullRom::Sample(float d, glm::vec3 &p, glm::vec3 &up)
{
	if (d < 0)
		return false;

	int M = (int) m_controlPoints.size();
	if (M == 0)
		return false;


	float fTotalLength = m_distances[m_distances.size() - 1];

	// The the current length along the control polygon; handle the case where we've looped around the track
	float fLength = d - (int) (d / fTotalLength) * fTotalLength;

	// Find the current segment
	int j = -1;
	for (int i = 0; i < (int)m_distances.size()-1; i++) {
		if (fLength >= m_distances[i] && fLength < m_distances[i + 1]) {
			j = i; // found it!
			break;
		}
	}

	if (j == -1)
		return false;

	// Interpolate on current segment -- get t
	float fSegmentLength = m_distances[j + 1] - m_distances[j];
	float t = (fLength - m_distances[j]) / fSegmentLength;
	
	// Get the indices of the four points along the control polygon for the current segment
	int iPrev = ((j-1) + M) % M;
	int iCur = j;
	int iNext = (j + 1) % M;
	int iNextNext = (j + 2) % M;

	// Interpolate to get the point (and upvector)
	p = Interpolate(m_controlPoints[iPrev], m_controlPoints[iCur], m_controlPoints[iNext], m_controlPoints[iNextNext], t);
	if (m_controlUpVectors.size() == m_controlPoints.size())
		up = glm::normalize(Interpolate(m_controlUpVectors[iPrev], m_controlUpVectors[iCur], m_controlUpVectors[iNext], m_controlUpVectors[iNextNext], t));

	return true;
}

/// <summary>
/// Sample with offset value
/// </summary>
/// <param name="d"></param>
/// <param name="p"></param>
/// <param name="up"></param>
/// <returns></returns>
bool CCatmullRom::SampleWithOffset(float d, glm::vec3& p, glm::vec3& up)
{
	if (d < 0)
		return false;

	int M = (int)m_controlPoints.size();
	if (M == 0)
		return false;


	float fTotalLength = m_distances[m_distances.size() - 1];

	// The the current length along the control polygon; handle the case where we've looped around the track
	float fLength = d - (int)(d / fTotalLength) * fTotalLength;

	// Find the current segment
	int j = -1;
	for (int i = 0; i < (int)m_distances.size() - 1; i++) {
		if (fLength >= m_distances[i] && fLength < m_distances[i + 1]) {
			j = i; // found it!
			break;
		}
	}

	if (j == -1)
		return false;

	// Interpolate on current segment -- get t
	float fSegmentLength = m_distances[j + 1] - m_distances[j];
	float t = (fLength - m_distances[j]) / fSegmentLength;

	// Get the indices of the four points along the control polygon for the current segment
	int iPrev = ((j - 1) + M) % M;
	int iCur = j;
	int iNext = (j + 1) % M;
	int iNextNext = (j + 2) % M;

	// Interpolate to get the point (and upvector)

	glm::vec3 offset = glm::vec3(0, 2.5f, 0);

	glm::vec3 p1 = m_controlPoints[iPrev] + offset;
	glm::vec3 p2 = m_controlPoints[iCur] + offset;
	glm::vec3 p3 = m_controlPoints[iNext] + offset;
	glm::vec3 p4 = m_controlPoints[iNextNext] + offset;

	p = Interpolate(p1, p2, p3, p4, t);
	if (m_controlUpVectors.size() == m_controlPoints.size())
		up = glm::normalize(Interpolate(m_controlUpVectors[iPrev], m_controlUpVectors[iCur], m_controlUpVectors[iNext], m_controlUpVectors[iNextNext], t));

	return true;
}

/// <summary>
/// Sample a set of control points using an open Catmull-Rom spline, to produce a set of iNumSamples that are (roughly) equally spaced
/// </summary>
/// <param name="numSamples"></param>
void CCatmullRom::UniformlySampleControlPoints(int numSamples)
{
	glm::vec3 p, up;

	// Compute the lengths of each segment along the control polygon, and the total length
	ComputeLengthsAlongControlPoints();
	float fTotalLength = m_distances[m_distances.size() - 1];

	// The spacing will be based on the control polygon
	float fSpacing = fTotalLength / numSamples;

	// Call PointAt to sample the spline, to generate the points
	for (int i = 0; i < numSamples; i++) {
		Sample(i * fSpacing, p, up);
		m_centrelinePoints.push_back(p);
		if (m_controlUpVectors.size() > 0)
			m_centrelineUpVectors.push_back(up);

	}


	// Repeat once more for truly equidistant points
	m_controlPoints = m_centrelinePoints;
	m_controlUpVectors = m_centrelineUpVectors;
	m_centrelinePoints.clear();
	m_centrelineUpVectors.clear();
	m_distances.clear();
	ComputeLengthsAlongControlPoints();
	fTotalLength = m_distances[m_distances.size() - 1];
	fSpacing = fTotalLength / numSamples;
	for (int i = 0; i < numSamples; i++) {
		Sample(i * fSpacing, p, up);
		m_centrelinePoints.push_back(p);
		if (m_controlUpVectors.size() > 0)
			m_centrelineUpVectors.push_back(up);
	}


}

/// <summary>
/// Create centreline of spline
/// </summary>
/// <param name="a_controlPoints"></param>
void CCatmullRom::CreateCentreline(const vector<glm::vec3>& a_controlPoints)
{
	//m_tex.Load("resources\\textures\\strip.png");
	//m_tex.SetSamplerObjectParameter(GL_TEXTURE_MIN_FILTER,
	//	GL_LINEAR_MIPMAP_LINEAR);
	//m_tex.SetSamplerObjectParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//m_tex.SetSamplerObjectParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
	//m_tex.SetSamplerObjectParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);

	// Call Set Control Points
	SetControlPoints(a_controlPoints);

	// Call UniformlySampleControlPoints with the number of samples required
	UniformlySampleControlPoints(350);

	/* UNCOMMENT THIS FOR THE CALCULATION OF VBO TO RENDER CENTERLINE
	// Create a VAO called m_vaoCentreline and a VBO to get the points onto the graphics card
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);
	// Create a VBO
	CVertexBufferObject m_vaoCentreline;
	m_vaoCentreline.Create();
	m_vaoCentreline.Bind();

	glm::vec2 texCoord1(0.0f, 0.0f);
	glm::vec2 l_arrVertices[4] = {

	glm::vec2 (0.0f, 0.0f),
	glm::vec2 (0.0f, 1.0f),
	glm::vec2 (1.0f, 0.0f),
	glm::vec2 (1.0f, 1.0f)
	};


	glm::vec3 normal(0.0f, 1.0f, 0.0f);

	int j = 0;

	for (unsigned int i = 0; i < m_controlPoints.size(); i++)
	{
		m_vaoCentreline.AddData(&m_controlPoints[i], sizeof(glm::vec3));
		m_vaoCentreline.AddData(&texCoord1, sizeof(glm::vec2));
		//m_vaoCentreline.AddData(&l_arrVertices[j], sizeof(glm::vec2));
		m_vaoCentreline.AddData(&normal, sizeof(glm::vec3));

		j= ++j % 4;
	}


		// Upload the VBO to the GPU
	m_vaoCentreline.UploadDataToGPU(GL_STATIC_DRAW);
	// Set the vertex attribute locations
	GLsizei stride = 2 * sizeof(glm::vec3) + sizeof(glm::vec2);
	// Vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, 0);
	// Texture coordinates
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride, (void*)sizeof(glm::vec3));
	// Normal vectors
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, stride, (void*)(sizeof(glm::vec3)
		+ sizeof(glm::vec2)));


	*/
}

/// <summary>
/// Create offset curves
/// </summary>
/// <param name="a_vector"></param>
void CCatmullRom::CreateOffsetCurves(glm::vec3 a_vector)
{
	// Compute the offset curves, one left, and one right.  Store the points in m_leftOffsetPoints and m_rightOffsetPoints respectively
	float w = 3;

	int l_iTotalControlPoints = (int) m_centrelinePoints.size();

	for (int i = 0; i < l_iTotalControlPoints; i++)
	{
		glm::vec3 p = m_centrelinePoints[i];

		/*float l_fltDistanceAhead = m_currentDistance + 1.0f;
		glm::vec3 pNext;

		Sample(l_fltDistanceAhead, pNext);*/

		glm::vec3 pNext = m_centrelinePoints[(i + 1)% l_iTotalControlPoints];

		glm::vec3 T = glm::normalize(pNext - p);

		glm::vec3 N = glm::normalize(glm::cross(T, a_vector));
	//	glm::vec3 N = glm::normalize(glm::cross(T, glm::vec3(0, 1, 0)));
		glm::vec3 B = glm::normalize(glm::cross(N, T));
		
		glm::vec3 l = p - (w / 2) * N;
		glm::vec3 r = p + (w / 2) * N;

		m_leftOffsetPoints.push_back(l);
		m_rightOffsetPoints.push_back(r);
	
	}

	// Generate two VAOs called m_vaoLeftOffsetCurve and m_vaoRightOffsetCurve, each with a VBO, and get the offset curve points on the graphics card
	// Note it is possible to only use one VAO / VBO with all the points instead.

	// left
	glGenVertexArrays(1, &m_vaoLeftOffsetCurve);
	glBindVertexArray(m_vaoLeftOffsetCurve);
	// Create a VBO
	CVertexBufferObject m_vboLeft;
	m_vboLeft.Create();
	m_vboLeft.Bind();
	glm::vec2 texCoord1(0.0f, 0.0f);
	glm::vec3 normal(0.0f, 1.0f, 0.0f);

	for (unsigned int i = 0; i < m_leftOffsetPoints.size(); i++)
	{
		m_vboLeft.AddData(&m_leftOffsetPoints[i], sizeof(glm::vec3));
		m_vboLeft.AddData(&texCoord1, sizeof(glm::vec2));
		m_vboLeft.AddData(&normal, sizeof(glm::vec3));
	}

	m_vboLeft.AddData(&m_leftOffsetPoints[0], sizeof(glm::vec3));
	m_vboLeft.AddData(&texCoord1, sizeof(glm::vec2));
	m_vboLeft.AddData(&normal, sizeof(glm::vec3));

	//m_vboLeft.AddData(&m_leftOffsetPoints[1], sizeof(glm::vec3));
	//m_vboLeft.AddData(&texCoord1, sizeof(glm::vec2));
	//m_vboLeft.AddData(&normal, sizeof(glm::vec3));


	// Upload the VBO to the GPU
	m_vboLeft.UploadDataToGPU(GL_STATIC_DRAW);
	// Set the vertex attribute locations
	GLsizei stride = 2 * sizeof(glm::vec3) + sizeof(glm::vec2);
	// Vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, 0);
	// Texture coordinates
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride, (void*)sizeof(glm::vec3));
	// Normal vectors
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, stride, (void*)(sizeof(glm::vec3)
		+ sizeof(glm::vec2)));


	// right
	glGenVertexArrays(1, &m_vaoRightOffsetCurve);
	glBindVertexArray(m_vaoRightOffsetCurve);
	// Create a VBO
	CVertexBufferObject m_vboRight;
	m_vboRight.Create();
	m_vboRight.Bind();
	glm::vec2 texCoordr(0.0f, 0.0f);
	glm::vec3 normalr(0.0f, 1.0f, 0.0f);

	for (unsigned int i = 0; i < m_rightOffsetPoints.size(); i++)
	{
		m_vboRight.AddData(&m_rightOffsetPoints[i], sizeof(glm::vec3));
		m_vboRight.AddData(&texCoordr, sizeof(glm::vec2));
		m_vboRight.AddData(&normalr, sizeof(glm::vec3));
	}

	m_vboRight.AddData(&m_rightOffsetPoints[0], sizeof(glm::vec3));
	m_vboRight.AddData(&texCoordr, sizeof(glm::vec2));
	m_vboRight.AddData(&normalr, sizeof(glm::vec3));

	//m_vboRight.AddData(&m_rightOffsetPoints[1], sizeof(glm::vec3));
	//m_vboRight.AddData(&texCoordr, sizeof(glm::vec2));
	//m_vboRight.AddData(&normalr, sizeof(glm::vec3));


	// Upload the VBO to the GPU
	m_vboRight.UploadDataToGPU(GL_STATIC_DRAW);
	// Set the vertex attribute locations
	GLsizei rstride = 2 * sizeof(glm::vec3) + sizeof(glm::vec2);
	// Vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, rstride, 0);
	// Texture coordinates
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, rstride, (void*)sizeof(glm::vec3));
	// Normal vectors
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, rstride, (void*)(sizeof(glm::vec3)
		+ sizeof(glm::vec2)));


}

/// <summary>
/// Create track
/// </summary>
/// <param name="a_strTexture"></param>
void CCatmullRom::CreateTrack(string a_strTexture)
{
	m_texRollarCoasterTrack.Load(a_strTexture);
	//m_texRollarCoasterTrack.Load("resources\\textures\\temp_tack_4.png");
	m_texRollarCoasterTrack.SetSamplerObjectParameter(GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR);
	m_texRollarCoasterTrack.SetSamplerObjectParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	m_texRollarCoasterTrack.SetSamplerObjectParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
	m_texRollarCoasterTrack.SetSamplerObjectParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	// Generate a VAO called m_vaoTrack and a VBO to get the offset curve points and indices on the graphics card
	// merge
	glGenVertexArrays(1, &m_vaoMerge);
	glBindVertexArray(m_vaoMerge);
	// Create a VBO
	CVertexBufferObject m_vboMerge;
	m_vboMerge.Create();
	m_vboMerge.Bind();

	glm::vec2 texCoord1(0.0f, 1.0f);
	glm::vec2 texCoord2(0.0f, 0.0f);
	glm::vec2 texCoord3(1.0f, 1.0f);
	glm::vec2 texCoord4(1.0f, 0.0f);

	glm::vec2 texCoordm(0.0f, 0.0f);
	glm::vec3 normalm(0.0f, 1.0f, 0.0f);

	m_rightOffsetPoints.push_back(m_rightOffsetPoints[0]);
	m_leftOffsetPoints.push_back(m_leftOffsetPoints[0]);

	m_vertexCount = m_rightOffsetPoints.size();

	for (unsigned int i = 0; i < m_vertexCount; i++)
	{
		m_vboMerge.AddData(&m_leftOffsetPoints[i + 1], sizeof(glm::vec3));
		m_vboMerge.AddData(&texCoord1, sizeof(glm::vec2));
		m_vboMerge.AddData(&normalm, sizeof(glm::vec3));

		m_vboMerge.AddData(&m_leftOffsetPoints[i], sizeof(glm::vec3));
		m_vboMerge.AddData(&texCoord2, sizeof(glm::vec2));
		m_vboMerge.AddData(&normalm, sizeof(glm::vec3));

		m_vboMerge.AddData(&m_rightOffsetPoints[i + 1], sizeof(glm::vec3));
		m_vboMerge.AddData(&texCoord3, sizeof(glm::vec2));
		m_vboMerge.AddData(&normalm, sizeof(glm::vec3));

		m_vboMerge.AddData(&m_rightOffsetPoints[i], sizeof(glm::vec3));
		m_vboMerge.AddData(&texCoord4, sizeof(glm::vec2));
		m_vboMerge.AddData(&normalm, sizeof(glm::vec3));
	}

	/*m_vboMerge.AddData(&m_leftOffsetPoints[0], sizeof(glm::vec3));
	m_vboMerge.AddData(&texCoord1, sizeof(glm::vec2));
	m_vboMerge.AddData(&normalm, sizeof(glm::vec3));

	m_vboMerge.AddData(&m_leftOffsetPoints[m_vertexCount - 1], sizeof(glm::vec3));
	m_vboMerge.AddData(&texCoord2, sizeof(glm::vec2));
	m_vboMerge.AddData(&normalm, sizeof(glm::vec3));

	m_vboMerge.AddData(&m_rightOffsetPoints[0], sizeof(glm::vec3));
	m_vboMerge.AddData(&texCoord3, sizeof(glm::vec2));
	m_vboMerge.AddData(&normalm, sizeof(glm::vec3));

	m_vboMerge.AddData(&m_rightOffsetPoints[m_vertexCount - 1], sizeof(glm::vec3));
	m_vboMerge.AddData(&texCoord4, sizeof(glm::vec2));
	m_vboMerge.AddData(&normalm, sizeof(glm::vec3));*/


	// Upload the VBO to the GPU
	m_vboMerge.UploadDataToGPU(GL_STATIC_DRAW);
	// Set the vertex attribute locations
	GLsizei mstride = 2 * sizeof(glm::vec3) + sizeof(glm::vec2);
	// Vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, mstride, 0);
	// Texture coordinates
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, mstride, (void*)sizeof(glm::vec3));
	// Normal vectors
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, mstride, (void*)(sizeof(glm::vec3)
		+ sizeof(glm::vec2)));
}

/// <summary>
/// Render centreline
/// </summary>
void CCatmullRom::RenderCentreline()
{
	// Bind the VAO m_vaoCentreline and render it

	glLineWidth(5.0f);
	glBindVertexArray(m_vao);
	//m_tex.Bind();

	glDrawArrays(GL_POINTS, 0, m_centrelinePoints.size());
	glDrawArrays(GL_LINE_LOOP, 0, m_centrelinePoints.size());

}

/// <summary>
/// Render offset curves
/// </summary>
void CCatmullRom::RenderOffsetCurves()
{
	// Bind the VAO m_vaoLeftOffsetCurve and render it
	glLineWidth(5.0f);
	glBindVertexArray(m_vaoLeftOffsetCurve);

	//std::cout << "Length"<< m_leftOffsetPoints.size();
	glDrawArrays(GL_POINTS, 0, m_leftOffsetPoints.size());
	glDrawArrays(GL_LINE_STRIP, 0, m_leftOffsetPoints.size());

	// Bind the VAO m_vaoRightOffsetCurve and render it

	glLineWidth(5.0f);
	glBindVertexArray(m_vaoRightOffsetCurve);

	glDrawArrays(GL_POINTS, 0, m_rightOffsetPoints.size());
	glDrawArrays(GL_LINE_STRIP, 0, m_rightOffsetPoints.size());


}

/// <summary>
/// Render track
/// </summary>
void CCatmullRom::RenderTrack()
{
	// Bind the VAO m_vaoTrack and render it

	// merge
	glLineWidth(5.0f);
	glBindVertexArray(m_vaoMerge);
	m_texRollarCoasterTrack.Bind();

	//glDrawArrays(GL_LINE_STRIP, 0, 100);

	int l_iFirst = 0;
	int l_iCount = 4;

	for (int i = 0; i < m_rightOffsetPoints.size() / 2; i++)
	{
		glDrawArrays(GL_TRIANGLE_STRIP, l_iFirst, l_iCount);
		/*glDrawArrays(GL_TRIANGLE_STRIP, first, count);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);*/
		l_iFirst = l_iCount;
		l_iCount += 4;
	}

	glDrawArrays(GL_TRIANGLE_STRIP, l_iFirst, l_iFirst);

}

/// <summary>
/// Return the currvent lap (starting from 0) based on distance along the control curve.
/// </summary>
/// <param name="d"></param>
/// <returns></returns>
int CCatmullRom::CurrentLap(float d)
{

	return (int)(d / m_distances.back());

}

/// <summary>
/// Dummy vector
/// </summary>
glm::vec3 CCatmullRom::_dummy_vector(0.0f, 0.0f, 0.0f);