#pragma once

#include "Common.h"
#include "GameWindow.h"
#include "RacePost.h"
#include "CatmullRom.h"
#include "PhysicSystem.h"
#include "CollisionObject.h"
#include "CollisionDetectionManager.h"
#include "RacePostHolder.h"
#include "ControlPoint.h"
#include "Collectable.h"
#include "Quad.h"
#include "FrameBufferObject.h"
#include "ControlPointManager.h"

// Classes used in game.  For a new class, declare it here and provide a pointer to an object of this class below.  Then, in Game.cpp, 
// include the header.  In the Game constructor, set the pointer to NULL and in Game::Initialise, create a new object.  Don't forget to 
// delete the object in the destructor.   
class CCamera;
class CSkybox;
class CShader;
class CShaderProgram;
class CPlane;
class CFreeTypeFont;
class CHighResolutionTimer;
class CSphere;
class COpenAssetImportMesh;
class CAudio;
class CCube;
enum class ECameraType;
class CollisionObject;
class CollisionDetectionManager;
class ControlPoint;
class Collectable;
class Quad;
class CFrameBufferObject;

extern "C" {
	_declspec(dllexport) DWORD NvOptimusEnablement = 0x0000001;      
}

class Game {
private:
	// Three main methods used in the game.  Initialise runs once, while Update and Render run repeatedly in the game loop.
	void Initialise();
	void Update();
	void Render();

	// Pointers to game objects.  They will get allocated in Game::Initialise()
	CSkybox *m_pSkybox;
	CCamera *m_pCamera;
	vector <CShaderProgram *> *m_pShaderPrograms;
	CFreeTypeFont * m_pFtFont;
	COpenAssetImportMesh *m_pBarrelMesh;
	CHighResolutionTimer *m_pHighResolutionTimer;
	CAudio *m_pAudio;

	// Some other member variables
	double m_dt;
	int m_framesPerSecond;
	bool m_appActive;

	double m_speed = 0.0f;

	//CCube* m_pCCube;

#pragma region COURSEWORK
	
	// *************** BASIC OBJECTS ******************

	/// <summary>
	/// Race post object to implement the primitive (BASIC OBJECT)
	/// </summary>
	CRacePost* m_pRacePost;

	/// <summary>
	/// Race post holder object to implement the primitive (BASIC OBJECT)
	/// </summary>
	CRacePostHolder* m_pCRacePostHolder;

	/// <summary>
	/// Render Race post and holder?
	/// </summary>
	bool m_hideRacePostNHolder = false;
	
	// ******************* TRACK *********************
	
	/// <summary>
	/// Control Point manager reference
	/// </summary>
	ControlPointManager* m_ControlPointManagerRef;
	
	/// <summary>
	/// Current train distance 
	/// </summary>
	float m_currentDistance = 0.0f;

	/// <summary>
	/// Camera rotation value over track
	/// </summary>
	float m_cameraRotation = 0.0f;

	// ***************** PHYSIC ***************************
	
	/// <summary>
	/// Physic system manager
	/// </summary>
	PhysicSystem* m_PhysicSystemRef;

	// ****************** HUD *******************************
	
	/// <summary>
	/// Fuel meter display
	/// </summary>
	CFreeTypeFont* m_pFuelMeter;

	/// <summary>
	/// Score display
	/// </summary>
	CFreeTypeFont* m_pScore;

	/// <summary>
	/// Train current velocity display
	/// </summary>
	CFreeTypeFont* m_pVelocity;

	/// <summary>
	/// Current threshold velocity display
	/// </summary>
	CFreeTypeFont* m_pThresholdVelocity;

	/// <summary>
	/// Threshold velocity at next point display
	/// </summary>
	CFreeTypeFont* m_pNextThresholdVelocity;

	/// <summary>
	/// Total score
	/// </summary>
	int m_iScore = 0;

	/// <summary>
	/// Current threshold velocity
	/// </summary>
	float m_fltThresholdVelocity = 0.0f;

	/// <summary>
	/// Threshold velocity at next point
	/// </summary>
	float m_fltNextThresholdVelocity = 0.0f;

	/// <summary>
	/// Train velocity
	/// </summary>
	float m_fltVelocityDisplay = 0.0f;

	// ****************** CAMERA ***************************

	/// <summary>
	/// Current camera view
	/// </summary>
	ECameraType m_ECameraType;;

	// ******************* MESH *****************************

	/// <summary>
	/// Train mesh
	/// </summary>
	COpenAssetImportMesh* m_pTrainMesh;

	/// <summary>
	/// Coin mesh
	/// </summary>
	COpenAssetImportMesh* m_pCoinMesh;

	/// <summary>
	/// Nitrous Mesh
	/// </summary>
	COpenAssetImportMesh* m_pNitrousMesh;

	/// <summary>
	/// 2 X nitrous Mesh
	/// </summary>
	COpenAssetImportMesh* m_pNitousDoubleMesh;

	/// <summary>
	/// All collectable rotation value
	/// </summary>
	float m_fltCollectableRotation = 0.0f;

	/// <summary>
	/// Mesh importor object to load and render racepost OBJ/MTL file
	/// </summary>
	COpenAssetImportMesh* m_pRacePostMesh;

	/// <summary>
	/// List of nitrous instance in the scene
	/// </summary>
	vector<Collectable*> m_lstNitro;

	/// <summary>
	/// List of 2 X nitrous instance in the scene
	/// </summary>
	vector<Collectable*> m_lstNitroDouble;

	/// <summary>
	/// Nitro fuel refill value
	/// </summary>
	const int FUEL_REFILL_FOR_SINGLE_NITRO = 5;

	/// <summary>
	/// 2 X Nitro fuel refill value
	/// </summary>
	const int FUEL_REFILL_FOR_DOUBLE_NITRO = 10;

	// ****************** TRAIN POSITION AND ORIENTATION ****************
	
	/// <summary>
	/// Current tarin position
	/// </summary>
	glm::vec3 m_vec3TrainPosition = glm::vec3(0, 0, 0);

	/// <summary>
	/// Current train orientation
	/// </summary>
	glm::mat4 PlayerRotation;

	/// <summary>
	/// Fall position of train after derail
	/// </summary>
	glm::vec3 m_vec3FallPosition = glm::vec3(0, 0, 0);

	/// <summary>
	/// Last train positio after derail
	/// </summary>
	glm::vec3 m_vec3LastTrainPosition = glm::vec3(0, 0, 0);

	// ********************* DERAIL *******************************

	/// <summary>
	/// Derial train
	/// </summary>
	void Derail();

	/// <summary>
	/// Is train derailed?
	/// </summary>
	bool m_isDerail = false;

	/// <summary>
	/// Derialed falling train rotation 
	/// </summary>
	float m_fltDerailRotation = 0.0f;

	/// <summary>
	/// Initiate derail?
	/// </summary>
	bool m_isInitiateDerail = false;

	/// <summary>
	/// Timer before derail
	/// </summary>
	float m_fltTimerBeforeDerial = 0.0f;

	/// <summary>
	/// Timer after derail
	/// </summary>
	float m_fltTimerAfterDerial = 0.0f;


	// ********************* COLLISIONS **********************

	/// <summary>
	/// Has lap started trigger
	/// </summary>
	CollisionObject* m_lapStarted;

	/// <summary>
	/// Reset all control points trigger
	/// </summary>
	CollisionObject* m_resetControlPoint;

	/// <summary>
	/// Collision detectio manager reference
	/// </summary>
	CollisionDetectionManager* m_CollisionDetectionManagerRef;

	/// <summary>
	/// Get current threshold velocity
	/// </summary>
	float GetCurrThresholdPoint();

	/// <summary>
	/// get next points threshold velocity
	/// </summary>
	float GetNextThresholdPoint();

	// ********************** CONSTANTS **************************

	/// <summary>
	/// Pie value
	/// </summary>
	const float PIE_VALUE = 3.14159f;

	// ********************** 2D AND ADVANCED TECHNEQUES**************************
	
	/// <summary>
	/// Quad to render vignette
	/// </summary>
	Quad* m_pQuad;

	/// <summary>
	/// FBO instance
	/// </summary>
	CFrameBufferObject* m_pFBO;

	/// <summary>
	/// Render function
	/// </summary>
	void RenderScene(int a_iPass);

	/// <summary>
	/// Show vignette?
	/// </summary>
	bool m_isShowVignette = false;

	/// <summary>
	/// Create FBO
	/// </summary>
	void CreateFBO();

	/// <summary>
	/// Is FBO created
	/// </summary>
	bool m_isFBOCreated = false;

	/// <summary>
	/// Game Window width
	/// </summary>
	int m_iWindowWidth = 0;

	/// <summary>
	/// Game window height
	/// </summary>
	int m_iWindowHeight = 0;

	/// <summary>
	/// Start Camera shake
	/// </summary>
	void StartCameraShake();

	/// <summary>
	/// Stop camera shake
	/// </summary>
	void StopCameraShake();

	/// <summary>
	/// Default rotation value of train before camera shake
	/// </summary>
	const float ROTATION_DEFAULT_VALUE = 0.0f;

	/// <summary>
	/// Max camera shake rotation
	/// </summary>
	const float ROTATION_MAX_VALUE = 0.01f;

	/// <summary>
	///  Min camera shake rotation
	/// </summary>
	const float ROTATION_MIN_VALUE = -0.01f;

	/// <summary>
	/// Diffrence between current velocity and threshold after which camera shake initiates
	/// </summary>
	const float THRESHOLD_DIFFERENCE_FOR_CAMERA_SHAKE = 0.10f;

	/// <summary>
	/// Is left camera shake completed?
	/// </summary>
	bool m_isFirstCycleRotation = false;
	
	// ********************** LIGHT **************************

	/// <summary>
	/// Light rotation max value
	/// </summary>
	const float MAX_VALUE = 0.6;

	/// <summary>
	/// Light rotation min value
	/// </summary>
	const float MIN_VALUE = 0.2;
	
	/// <summary>
	/// Current light rotation value spotlight 1 (Red)
	/// </summary>
	float m_fltLightRotation = MIN_VALUE;

	/// <summary>
	/// Current light rotation value spotlight 2 (Green)
	/// </summary>
	float m_fltReverseRotation = MAX_VALUE;

	/// <summary>
	/// Is one rotation completed of lights?
	/// </summary>
	bool m_isFadeIn = false;


	// ********************** END SCREEN **************************
	
	/// <summary>
	/// Show end screen?
	/// </summary>
	bool m_isShowEndScreen = false;
	
	/// <summary>
	/// To render end screen
	/// </summary>
	Quad* m_pEndScreen;

	/// <summary>
	/// Timer for no fuel no velocity conidtion
	/// </summary>
	float m_timerForNoFuelNoVelocity = 0.0f;

	// ********************** TESTING **************************

	/// <summary>
	/// Is Testing?
	/// </summary>
	bool m_isTesting = false;

#pragma endregion

public:
	Game();
	~Game();
	static Game& GetInstance();
	LRESULT ProcessEvents(HWND window,UINT message, WPARAM w_param, LPARAM l_param);
	void SetHinstance(HINSTANCE hinstance);
	WPARAM Execute();

private:
	static const int FPS = 60;
	void DisplayFrameRate();
	void GameLoop();
	GameWindow m_gameWindow;
	HINSTANCE m_hInstance;
	int m_frameCount;
	double m_elapsedTime;


};

#pragma region COURSEWORK

/// <summary>
/// Camera angles
/// </summary>
enum class ECameraType
{
	FirstPeron, ThirdPerson, TopView
};

#pragma endregion
