#include "Cube.h"
CCube::CCube()
{}
CCube::~CCube()
{
	Release();
}
void CCube::Create(string filename)
{
	m_texture.Load(filename);
	m_texture.SetSamplerObjectParameter(GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR);
	m_texture.SetSamplerObjectParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	m_texture.SetSamplerObjectParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
	m_texture.SetSamplerObjectParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);
	m_vbo.Create();
	m_vbo.Bind();
	// Write the code to add interleaved vertex attributes to the VBO
	
	glm::vec3 l_p0 = glm::vec3(-1.0f, -1.0f, -1.0f);
	glm::vec3 l_p1 = glm::vec3(1.0f, -1.0f, -1.0f);
	glm::vec3 l_p2 = glm::vec3(-1.0f, 1.0f, -1.0f);
	glm::vec3 l_p3 = glm::vec3(1.0f, 1.0f, -1.0f);
	glm::vec3 l_p4 = glm::vec3(-1.0f, -1.0f, 1.0f);
	glm::vec3 l_p5 = glm::vec3(1.0f, -1.0f, 1.0f);
	glm::vec3 l_p6 = glm::vec3(-1.0f, 1.0f, 1.0f);
	glm::vec3 l_p7 = glm::vec3(1.0f, 1.0f, 1.0f);


	// Vertex positions
	glm::vec3 planeVertices[20] =
	{
		l_p0,
		l_p2,
		l_p1,
		l_p3,

		l_p1,
		l_p3,
		l_p5,
		l_p7,

		l_p5,
		l_p7,
		l_p4,
		l_p6,

		l_p4,
		l_p6,
		l_p0,
		l_p2,

		l_p2,
		l_p6,
		l_p3,
		l_p7,

	

		/*glm::vec3(-1.0f, -1.0f, -1.0f),
		glm::vec3(-1.0f, 1.0f, -1.0f),
		glm::vec3(1.0f, -1.0f, -1.0f),
		glm::vec3(1.0f, 1.0f, -1.0f),

		glm::vec3(1.0f, -1.0f, -1.0f),
		glm::vec3(1.0f, 1.0f, -1.0f),
		glm::vec3(1.0f, -1.0f, 1.0f),
		glm::vec3(1.0f, 1.0f, 1.0f),

		glm::vec3(1.0f, -1.0f, 1.0f),
		glm::vec3(1.0f, 1.0f, 1.0f),
		glm::vec3(-1.0f, -1.0f, 1.0f),
		glm::vec3(-1.0f, 1.0f, 1.0f),

		glm::vec3(-1.0f, -1.0f, 1.0f),
		glm::vec3(-1.0f, 1.0f, 1.0f),
		glm::vec3(-1.0f, -1.0f, -1.0f),
		glm::vec3(-1.0f, 1.0f, -1.0f),

		glm::vec3(-1.0f, 1.0f, -1.0f),
		glm::vec3(-1.0f, 1.0f, 1.0f),
		glm::vec3(1.0f, 1.0f, -1.0f),
		glm::vec3(1.0f, 1.0f, 1.0f),*/

		/*glm::vec3(-1.0f, 1.0f, 1.0f),
		glm::vec3(1.0f, 1.0f, 1.0f),
		glm::vec3(0.5f, 2.0f, 1.0f),*/
	};

	// Texture coordinates
	glm::vec2 planeTexCoords[20] =
	{
		glm::vec2(0.0f, 0.0f),
		glm::vec2(0.0f, 1.0f),
		glm::vec2(1.0f, 0.0f),
		glm::vec2(1.0f, 1.0f),


		glm::vec2(0.0f, 0.0f),
		glm::vec2(0.0f, 1.0f),
		glm::vec2(1.0f, 0.0f),
		glm::vec2(1.0f, 1.0f),

		glm::vec2(0.0f, 0.0f),
		glm::vec2(0.0f, 1.0f),
		glm::vec2(1.0f, 0.0f),
		glm::vec2(1.0f, 1.0f),

		glm::vec2(0.0f, 0.0f),
		glm::vec2(0.0f, 1.0f),
		glm::vec2(1.0f, 0.0f),
		glm::vec2(1.0f, 1.0f),

		glm::vec2(0.0f, 0.0f),
		glm::vec2(0.0f, 1.0f),
		glm::vec2(1.0f, 0.0f),
		glm::vec2(1.0f, 1.0f),

	/*	glm::vec2(0.0f, 0.0f),
		glm::vec2(0.0f, 1.0f),
		glm::vec2(0.5f, 0.5f)*/
	};

	// Plane normal
	glm::vec3 planeNormal = glm::vec3(0.0f, 0.0f, 1.0f);

	glm::vec3 planeNormal1 = glm::vec3(1.0f, 0.0f, 0.0f);


	//glm::vec3 nf1 = glm::vec3(0.0f, 0.0f, -1.0f);
	//glm::vec3 nf2 = glm::vec3(-1.0f, 0.0f, 0.0f);
	//glm::vec3 nf3 = glm::vec3(0.0f, 0.0f, -1.0f);
	//glm::vec3 nf4 = glm::vec3(-1.0f, 0.0f, 0.0f);
	//glm::vec3 nf5 = glm::vec3(0.0f, 1.0f, 0.0f);

	glm::vec3 nf1 = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 nf2 = glm::vec3(1.0f, 0.0f, 0.0f);
	glm::vec3 nf3 = glm::vec3(0.0f, 0.0f, 1.0f);
	glm::vec3 nf4 = glm::vec3(-1.0f, 0.0f, 0.0f);
	glm::vec3 nf5 = glm::vec3(0.0f, 1.0f, 0.0f);


	glm::vec3 arrNormals[20] =
	{
		// face 1
		nf1,nf1,nf1,nf1,

		// face 2
		nf2,nf2,nf2,nf2,

		// face 3
		nf3,nf3,nf3,nf3,

		// face 4
		nf4,nf4,nf4,nf4,

		// face 5
		nf5,nf5,nf5,nf5,

	};



	// Put the vertex attributes in the VBO
	for (unsigned int i = 0; i < 20; i++) {
		m_vbo.AddData(&planeVertices[i], sizeof(glm::vec3));
		m_vbo.AddData(&planeTexCoords[i], sizeof(glm::vec2));
		m_vbo.AddData(&arrNormals[i], sizeof(glm::vec3));
		//m_vbo.AddData(&planeNormal, sizeof(glm::vec3));
	}

	// Upload data to GPU
	m_vbo.UploadDataToGPU(GL_STATIC_DRAW);

//	glFrontFace(GL_CW);
	GLsizei stride = 2 * sizeof(glm::vec3) + sizeof(glm::vec2);
	// Vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, 0);
	// Texture coordinates
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride, (void*)sizeof(glm::vec3));
	// Normal vectors
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, stride,
		(void*)(sizeof(glm::vec3) + sizeof(glm::vec2)));
}
void CCube::Render()
{
	glBindVertexArray(m_vao);
	m_texture.Bind();
	// Call glDrawArrays to render each side
	//glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 4, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 8, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 12, 4);

	glDrawArrays(GL_TRIANGLE_STRIP, 16, 4);

	//glDrawArrays(GL_TRIANGLE_STRIP, 20, 3);



}
void CCube::Release()
{
	m_texture.Release();
	glDeleteVertexArrays(1, &m_vao);
	m_vbo.Release();
}